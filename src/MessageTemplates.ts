import { Singleton } from 'typescript-ioc';

@Singleton
export class MessageTemplates {
    startMessage: string;
    okMessage: string;
    errMessage: string;
    cancelMessage: string;
    infoMessage: string;
}