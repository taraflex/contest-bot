import { Column, Entity, Index, PrimaryColumn, Repository } from 'typeorm';

import { Access, v } from '@crud';
import { RTTIItemState } from '@crud/types';

@Access({ GET: 0, DELETE: 0, PATCH: 0 }, { display: 'table', icon: 'bullhorn', order: 10 })
@Entity()
export class Channel {
    @v({ state: RTTIItemState.READONLY })
    @Column({ default: '' })
    title: string;

    @v({ state: RTTIItemState.READONLY })
    @Column({ default: '' })
    link: string;

    @v({ state: RTTIItemState.HIDDEN })
    @Column({ type: 'bigint', default: 0 })
    chat_id: number;

    @v({ state: RTTIItemState.HIDDEN })
    @PrimaryColumn({ type: 'int' })
    supergroup_id: number;

    @Index()
    @Column({ default: false })
    required: boolean;
}

export abstract class ChannelRepository extends Repository<Channel>{ }