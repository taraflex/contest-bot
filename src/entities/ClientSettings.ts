import { Column, PrimaryGeneratedColumn, Repository } from 'typeorm';

import { Access, v } from '@crud';
import { SingletonEntity } from '@ioc';
import { API_HASH_RE, BOT_TOKEN_RE } from '@utils/tg-utils';

@Access({ GET: 0, PATCH: 0 }, { display: 'single', icon: 'cog', order: 4 }, { PATCH: 'tgclient_save' })
@SingletonEntity()
export class ClientSettings {
    @PrimaryGeneratedColumn()
    id: number;

    @v({ notEqual: 0, description: 'Api id взять [тут](https://my.telegram.org/apps)' })
    @Column({ type: 'int', default: 0 })
    apiId: number;

    @v({ pattern: API_HASH_RE, description: 'Api hash взять [там же](https://my.telegram.org/apps)' })
    @Column({ default: '', length: 32 })
    apiHash: string;

    @v({ pattern: BOT_TOKEN_RE, description: 'Токен телеграм бота [ℹ️ справка](https://www.youtube.com/watch?v=W0f66uie9e8)' })
    @Column({ default: '' })
    token: string;

    @v({ type: 'md', description: `Приветственное сообщение` })
    @Column({
        default: `Список каналов и групп для участия:
{{#chats}}
- [{{{title}}}]({{{link}}})
{{/chats}}` })
    startMessage: string;

    @v({ type: 'md', description: `Сообщение, если пользователь подписался на все требуемые группы/каналы и нажал ***участвовать***.` })
    @Column({ default: `Теперь вы будете учавствовать в конкурсах.` })
    okMessage: string;

    @v({ type: 'md', description: `Сообщение, если пользователь нажал ***участвовать***, но не подписался на все требуемые группы.` })
    @Column({
        default: `Видимо вы подписались не на все требуемые каналы или группы для участия в конкурсах.
Список каналов и групп для участия:
{{#chats}}
- [{{{title}}}]({{{link}}})
{{/chats}}` })
    errMessage: string;

    @v({ type: 'md', description: `Сообщение, если пользователь отказался от участия в конкурсах.` })
    @Column({ default: `Вы больше не будете учавствовать в конкурсах.` })
    cancelMessage: string;

    @v({ type: 'md', description: `Информационное сообщение при изменении необходимого состава каналов и групп (не будет выдаваться, если пользователь уже подписан на все новые каналы/группы)` })
    @Column({
        default: `Список каналов и групп для участия изменен. Актуальный список:
{{#chats}}
- [{{{title}}}]({{{link}}})
{{/chats}}
Нажмите *Не хочу учавствовать* для отмены информационных сообщений.` })
    infoMessage: string;
}

export abstract class ClientSettingsRepository extends Repository<ClientSettings>{ }