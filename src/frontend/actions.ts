import { every } from 'visibilityjs';
import Component, { mixins } from 'vue-class-component';

import { ErrorHandlerMixin } from '@frontend/error-handler-mixin';

@Component
export default class Actions extends mixins(ErrorHandlerMixin) {
    static get icon() {
        return 'qrcode';
    }
    contestUrl: string = location.origin + Paths.members;
    queueSize: number = 0;

    mounted() {
        every(3000, () => {
            this.$rpc.remote('notify').getQueueSize()
                .then(v => {
                    this.queueSize = v;
                })
                .catch(err => LOG_ERROR(err));
        });
    }

    async brodcast() {
        this.loading = true;
        try {
            this.queueSize = await this.$rpc.remote('notify').brodcast();
        } catch (err) {
            this.onError(err);
        } finally {
            this.loading = false;
        }
    }

    async cancelBrodcast() {
        this.loading = true;
        try {
            this.queueSize = await this.$rpc.remote('notify').cancelBrodcast();
        } catch (err) {
            this.onError(err);
        } finally {
            this.loading = false;
        }
    }
}