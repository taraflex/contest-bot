import { Context } from 'koa';
import * as TelegramBot from 'node-telegram-bot-api';
import pMap from 'p-map';
import { Options } from 'request';
import {
    chatMember, messageText, Update, updateNewMessage, updateSupergroup, updateSupergroupFullInfo
} from 'tdl/types/tdlib';
import timestamp from 'time-stamp';
import { Inject, Singleton } from 'typescript-ioc';
import XLSXWriter from 'xlsx-writestream';

import { Channel, ChannelRepository } from '@entities/Channel';
import noStore from '@middlewares/no-store';
import { delay } from '@taraflex/debounce';
import { AsyncQueue } from '@utils/AsyncQueue';
import { BaseRequestOpts } from '@utils/cancelable-request';
import { get } from '@utils/routes-helpers';
import { sendErrorEmail } from '@utils/send-email';

import { isCantWriteError } from './core/BaseTelegramBot';
import { BaseTelegramClient, BotAuthInfo, UserAuthInfo } from './core/BaseTelegramClient';
import { MessageTemplates } from './MessageTemplates';
import { UsersStorage } from './UsersStorage';

@Singleton
export class ContestBot extends BaseTelegramClient {

    protected readonly storage = new UsersStorage();
    protected bot: TelegramBot;
    protected readonly chain = new AsyncQueue<{ chat_id: number, message: string }>();
    protected readonly gchain = new AsyncQueue<{ supergroup_id: number, link: string }>();

    constructor(
        @Inject private readonly channelRepository: ChannelRepository,
        @Inject private readonly messages: MessageTemplates
    ) {
        super();
        this.thread().catch(err => LOG_ERROR(err));
        this.groupInfoThread().catch(err => LOG_ERROR(err));
    }

    protected async thread() {
        for await (let { chat_id, message } of this.chain) {
            try {
                const s = Date.now();
                await this.bot.sendMessage(chat_id, message, {
                    parse_mode: 'Markdown',
                    reply_markup: {
                        keyboard: [[{ text: 'Хочу учавствовать' }, { text: 'Не хочу учавствовать' }]],
                        resize_keyboard: true
                    },
                    disable_web_page_preview: true
                });
                const pause = 1000 / 30 - (Date.now() - s);
                if (pause > 0) {
                    await delay(pause);
                }
            } catch (err) {
                if (isCantWriteError(err)) {
                    this.storage.remove(chat_id);
                } else {
                    LOG_ERROR(err);
                }
            }
        }
    }

    async update(authInfo: UserAuthInfo | BotAuthInfo, ctx?: Context) {
        if (await super.update(authInfo, ctx)) {
            this.bot = new TelegramBot(this.phoneOrToken, { request: <Options>BaseRequestOpts });
            await pMap(await this.channelRepository.find(), async c => {
                try {
                    const { title, id } = await this.client.invoke({
                        _: 'createSupergroupChat',
                        supergroup_id: c.supergroup_id,
                    });
                    c.title = title || c.title;
                    c.chat_id = id;
                } catch (err) {
                    LOG_2ERRORS(c, err);
                    if (c.required) {
                        sendErrorEmail(`Ошибка запроса информации по чату:\n${JSON.stringify(c, null, '    ')}\nЧат будет исключен из обязательных.`).catch(err => LOG_ERROR(err));
                        c.required = false;
                    }
                }
                await this.channelRepository.save(c);
            }, { concurrency: 5 });
            return true;
        }
        return false;
    }

    protected actualChannels() {
        return this.channelRepository.find({ required: true });
    }

    protected async findResp(s: string, id: number) {
        try {
            this.chain.delete(v => v.chat_id === id);
            switch (s) {
                case 'Не хочу учавствовать':
                    this.storage.remove(id);
                    return this.messages.cancelMessage;
                case 'Хочу учавствовать':
                    //todo queue getChatMember
                    const members = await Promise.all((await this.actualChannels()).map(c =>
                        this.client.invoke({ _: 'getChatMember', chat_id: c.chat_id, user_id: id })));

                    if (members.some(m => m.status._ === 'chatMemberStatusLeft')) {
                        return this.messages.errMessage;
                    } else {
                        this.storage.add(id);
                        return this.messages.okMessage;
                    }
                default:
                    return this.messages.startMessage;
            }
        } catch (e) {
            LOG_2ERRORS(id, e);
            return 'Непредвиденная ошибка. Попробуйте позднее.';
        }
    }

    protected async chatUsers(supergroup_id: number) {
        let users: chatMember[] = [];
        for (let offset = 0; ; offset += 200) {
            const { members, total_count } = await this.client.invoke({
                _: 'getSupergroupMembers',
                supergroup_id,
                filter: { _: 'supergroupMembersFilterRecent' },
                offset,
                limit: 200,
            });
            if (!members) {
                break;
            }
            if (members.length > 0) {
                users.push(...members);
            }
            if (total_count <= 200 || members.length < 200 || users.length >= total_count) {
                break;
            }
            await delay(1000 / 30);
        }
        return users.filter(u => u && !u.bot_info && u.status._ !== 'chatMemberStatusLeft').map(u => u.user_id);
    }

    get queueSize() {
        return this.chain.size;
    }

    protected async allChatsUsers() {
        const users: Set<number>[] = [];
        for (let { supergroup_id } of await this.actualChannels()) {
            users.push(new Set(await this.chatUsers(supergroup_id)));
        }
        return users;
    }

    async brodcast() {
        const users = await this.allChatsUsers();
        this.cancelBrodcast();
        for (let user_id of this.storage.users) {
            if (!users.every(s => s.has(user_id))) {
                this.chain.add({
                    chat_id: user_id,
                    message: this.messages.infoMessage
                }, 10, true);
            }
        }
        return this.chain.size;
    }

    cancelBrodcast() {
        this.chain.filter((_, priority) => priority < 10);
        return this.chain.size;
    }

    @get({ name: 'members' }, noStore)
    async chatsMembers(ctx: Context) {
        const writer = new XLSXWriter();
        try {
            const users = await this.allChatsUsers();

            ctx.attachment(`users.${timestamp('YYYY.MM.DD[HH.mm]')}.xlsx`);
            ctx.body = writer.getReadStream();

            writer.defineColumns([
                { width: 30 },
                { width: 30 },
                { width: 30 }
            ]);

            for (let user_id of this.storage.users) {
                if (users.every(s => s.has(user_id))) {
                    const { first_name, last_name, username } = await this.client.invoke({ _: 'getUser', user_id });
                    writer.addRow({
                        first_name,
                        last_name,
                        link: username ? { value: '@' + username, hyperlink: 'https://t.me/' + username } : '',
                    });
                }
            }

            writer.finalize();
        } catch (err) {
            writer.dispose();
            LOG_ERROR(err);
            throw err;
        }
    }

    async groupInfoThread() {
        for await (let { supergroup_id, link } of this.gchain) {
            try {
                let needSave = false;
                const s = (await this.channelRepository.findOne({ supergroup_id })) || new Channel();
                if (s.supergroup_id !== supergroup_id) {
                    s.supergroup_id = supergroup_id;
                    needSave = true;
                }
                if (!s.title || !s.chat_id) {
                    try {
                        const { title, id } = await this.client.invoke({
                            _: 'createSupergroupChat',
                            supergroup_id,
                        });
                        s.chat_id = id;
                        s.title = title;
                        needSave = true;
                    } catch (err) {
                        LOG_2ERRORS(supergroup_id, err);
                    }
                }
                link = link || s.link;
                if (!link || !link.startsWith('https://t.me/joinchat/')) {
                    try {
                        link = (await this.client.invoke({
                            _: 'getSupergroupFullInfo',
                            supergroup_id
                        })).invite_link || link;
                    } catch (err) {
                        LOG_2ERRORS(supergroup_id, err);
                    }
                    if (s.chat_id && (!link || !link.startsWith('https://t.me/joinchat/'))) {
                        try {
                            link = (await this.client.invoke({
                                _: 'generateChatInviteLink',
                                chat_id: s.chat_id,
                            })).invite_link || link;
                        } catch (err) {
                            LOG_2ERRORS(supergroup_id, err);
                        }
                    }
                }
                if (link && s.link !== link) {
                    s.link = link;
                    needSave = true;
                }
                if (needSave) {
                    await this.channelRepository.save(s, { reload: false, listeners: false });
                }
            } catch (err) {
                LOG_2ERRORS(supergroup_id, err);
            }
        }
    }

    protected async onUpdate(update: Update) {
        switch (update._) {
            case 'updateNewMessage':
                const { message } = update as updateNewMessage;
                if (message.sender_user_id === message.chat_id && !message.is_outgoing) {
                    const { text } = message.content as messageText;
                    if (text) {
                        this.chain.add({
                            chat_id: message.chat_id,
                            message: await this.findResp(text.text, message.sender_user_id)
                        }, 0);
                    }
                }
                break;
            case 'updateSupergroupFullInfo':
                const { supergroup_full_info, supergroup_id } = update as updateSupergroupFullInfo;
                if (supergroup_full_info.can_get_members && supergroup_full_info.invite_link) {
                    setTimeout(() => this.gchain.add({
                        supergroup_id,
                        link: supergroup_full_info.invite_link
                    }, 0), 2000).unref();
                }
                break;
            case 'updateSupergroup':
                const { supergroup } = update as updateSupergroup;
                const link = supergroup.username ? 'https://t.me/' + supergroup.username : ''
                setTimeout(() => this.gchain.add({
                    supergroup_id: supergroup.id,
                    link
                }, link ? 0 : 10), 2000).unref();
                break;
        }
    }
}