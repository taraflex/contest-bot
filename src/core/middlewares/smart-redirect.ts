import { Context } from 'koa';
import * as Router from 'koa-router';

import resolveUrl from '@utils/resolve-url';
import rndId from '@utils/rnd-id';

import { setNoStoreHeader } from './no-store';

declare module 'koa' {
    interface Context {
        router: Router;
        namedRedirect: (routName: string, delay?: number) => void;
        resolve: (routName: string) => string;
    }
}

const routsCache = Object.create(null);
const secureRoutsCache = Object.create(null);

const ID_DUMMY = `__${rndId()}__`;
const ID_RE = new RegExp(String.raw`/?${ID_DUMMY}`, 'gi');

export default (ctx: Context, next: () => Promise<any>) => {

    const rcache = ctx.secure ? secureRoutsCache : routsCache;

    ctx.resolve = routeName => rcache[routeName] || (rcache[routeName] = resolveUrl(ctx, ctx.router.url(routeName, { id: ID_DUMMY })).replace(ID_RE, ''));

    ctx.namedRedirect = (routName, delay) => {
        setNoStoreHeader(ctx);
        //todo if not installed redirect to 'install' route
        const target = ctx.resolve(routName);
        if (delay > 0) {
            ctx.status = 500;
            ctx.type = 'html';
            //в chrome баг, который не дает установить куку при редиректе через location или refresh приходится извращаться
            ctx.body = `<html><head><meta http-equiv="refresh" content="${delay};url=${target}" /></head></html>`;
        } else {
            ctx.redirect(target);
        }
    };

    return next();
}