import { Context } from 'koa';

import { PORT } from '@utils/config';
import { deleteCookie, getCookie, updateCookie } from '@utils/server-cookie';

export const enum FlashLevel {
    ERROR = 0,
    WARNING = 1,
    INFO = 2,
}

declare module 'koa' {
    interface Context {
        flash?: FlashData;
        addFlash?: (message: string, level?: FlashLevel) => void;
    }
}

const FLASH_COOKIE_LITERAL = '_f' + PORT;

export default async (ctx: Context, next: () => Promise<any>) => {
    const t = getCookie(FLASH_COOKIE_LITERAL, ctx);
    if (t) {
        ctx.flash = {
            messages: t[FlashLevel.INFO],
            warnings: t[FlashLevel.WARNING],
            errors: t[FlashLevel.ERROR]
        };
    }

    let a: string[][] = [[], [], []];

    ctx.addFlash = (message: string, level?: FlashLevel) => {
        a[level | 0].push(message);
    }

    await next();

    if (a.some(v => v.length > 0)) {
        updateCookie(FLASH_COOKIE_LITERAL, ctx, a);
    } else if (t) {
        deleteCookie(FLASH_COOKIE_LITERAL, ctx);
    }
}