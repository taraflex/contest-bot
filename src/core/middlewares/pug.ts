import * as etag from 'etag';
import { readFileSync } from 'fs';
import { Context } from 'koa';
import { compileTemplate, LocalsObject } from 'pug';

import { STATIC } from '@utils/config';
import resolveUrl from '@utils/resolve-url';

declare module 'koa' {
    interface Context {
        pug: (file?: string | compileTemplate, locals?: LocalsObject, noCommon?: boolean) => void;
    }
}

const manifest: { [_: string]: { css?: string, js?: string } } = JSON.parse(readFileSync('./build/webpack-assets.json', 'utf-8'));

export default (assetsUrl?: string) => function (ctx: Context, next: () => Promise<any>) {
    ctx.pug = (file?: string | compileTemplate, locals?: LocalsObject, noCommon?: boolean) => {

        ctx.type = 'html';
        const template: compileTemplate = (typeof file === 'function') ? file : require('../templates/' + file + '.pug');

        const entry = locals && locals.entry && manifest[locals.entry];
        const styles = [DEBUG || noCommon ? null : 'common.css', entry && entry.css];
        const scripts = [!DEBUG || noCommon ? null : 'common.js', entry && entry.js];

        const body = template({
            title: APP_TITLE,
            assetsUrl: assetsUrl || (assetsUrl = resolveUrl(ctx, STATIC, DEBUG)),
            favicon: 'favicon.ico',
            ...ctx.flash,
            ...locals,
            styles,
            scripts
        });

        ctx.set('Cache-Control', 'no-cache, max-age=31536000');
        const et = etag(body);
        ctx.set('ETag', et);
        if (ctx.headers['if-none-match'] && ctx.headers['if-none-match'].endsWith(et)) {
            ctx.status = 304;
        } else {
            ctx.body = body;
        }
    };
    return next();
}