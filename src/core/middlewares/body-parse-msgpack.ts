import { Context } from 'koa';
import { decode } from 'msgpack-lite';
import * as rawBody from 'raw-body';

import options from '@utils/msgpack-options';

export const MAX_PAYLOAD_SIZE = 1024 * 1024 * 5;

export default async (ctx: Context, next: () => Promise<any>) => {
    const { method } = ctx.request
    if (method === 'PATCH' || method === 'PUT' || method === 'POST') {
        const body = await rawBody(ctx.req, { limit: MAX_PAYLOAD_SIZE });
        ctx.request.body = Object.assign((body && body.length > 0 ? decode(body, options) : null), ctx.request.query);
    }
    return next();
}