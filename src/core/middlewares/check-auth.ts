import { Context } from 'koa';

import isAjax from '@utils/is-ajax';

export default function (ctx: Context, next: () => Promise<any>) {
    if (ctx.isAuthenticated()) {
        return next();
    } else if (isAjax(ctx)) {
        throw 401;
    } else {
        ctx.namedRedirect('login');
    }
}