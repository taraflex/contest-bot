import { Context } from 'koa';

import isAjax from '@utils/is-ajax';
import stringify from '@utils/stringify';

import { destroySession } from '../AuthRegister';

export default (redirectRouteName: string) => async function (ctx: Context, next: () => Promise<any>) {
    if (isAjax(ctx)) {
        await next();
    } else {
        try {
            await next();
        } catch (err) {
            if (err instanceof Error) {
                LOG_ERROR(err);
                destroySession(ctx);
                ctx.namedRedirect(redirectRouteName, 1);
            } else {
                if (err && ctx.addFlash) {
                    if (typeof (err.forEach) === 'function') {
                        err.forEach(e => ctx.addFlash(stringify(e)));
                    } else {
                        ctx.addFlash(stringify(err));
                    }
                }
                ctx.namedRedirect(redirectRouteName);
            }
        }
    }
}