import * as koa from 'koa';
import { Inject, Singleton } from 'typescript-ioc';

import { Settings } from '@entities/Settings';

@Singleton
export class Koa extends koa {
    constructor(@Inject settings: Settings) {
        super();
        this.proxy = true;
        //load secret from persistent storage (required by koa-session)
        this.keys = [settings.secret];
    }
}