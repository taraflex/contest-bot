import * as natsort from 'alphanum-sort';
import * as fg from 'fast-glob';
import { readFile, unlink, writeFile } from 'fs-extra';
import * as SQL from 'sql.js';
import timestamp from 'time-stamp';
import { Connection, createConnection, QueryRunner } from 'typeorm';
import { SqljsDriver } from 'typeorm/driver/sqljs/SqljsDriver';
import { Provided } from 'typescript-ioc';

import { entities, repositories } from '@entities';
import { createAsync } from '@ioc/singleton-providers-factory';
import subscribers from '@subscribers';
import { trottleAsync } from '@taraflex/debounce';
import { AUTOSAVE, AUTOSAVE_INTERVAL } from '@utils/config';
import dataPath from '@utils/data-path';

export function createDBFilename() {
    return APP_NAME + '.' + timestamp('YYYY.MM.DD[HH.mm.ss]') + '.sqlite3';
}

async function generateMigrations(connection: Connection): Promise<Function[]> {
    const { upQueries, downQueries } = await connection.driver.createSchemaBuilder().log();
    if (upQueries.length > 0) {
        const migration = new Function(`return function _${Date.now()}() { }`)();
        //todo продумать, как определить миграции когда меняются только индексы
        //создаем миграцию если число DROP === CREATE и нет никаких других команд 
        const s = upQueries.reduce((s, q) => s + +q.startsWith('DROP INDEX '), 0);
        if (s !== upQueries.length / 2 || s !== upQueries.reduce((s, q) => s + +q.startsWith('CREATE INDEX '), 0)) {
            migration.prototype.up = async function (queryRunner: QueryRunner) {
                console.log('Start migration:');
                for (let q of upQueries) {
                    console.log(q);
                    await queryRunner.query(q);
                }
            };
            migration.prototype.down = async function (queryRunner: QueryRunner) {
                console.log('Rollback migration:');
                for (let q of downQueries) {
                    console.log(q);
                    await queryRunner.query(q);
                }
            };
            return [migration];
        }
    }
    return [];
}

function getConnection(database: Buffer, sqliteFilename: string, migrations?: Function[]) {
    return createConnection({
        type: 'sqljs',
        database,
        autoSave: AUTOSAVE,
        autoSaveCallback: AUTOSAVE ? trottleAsync(async db => {
            if (db instanceof SQL.Database) {
                await writeFile(sqliteFilename, db.export());
            }
        }, AUTOSAVE_INTERVAL, err => LOG_ERROR(err)) : undefined,
        synchronize: !database,
        logging: false,
        entities: entities as Function[],
        subscribers: subscribers as Function[],
        migrationsRun: Array.isArray(migrations) && migrations.length > 0,
        migrations: migrations
    });
}

const provider = createAsync(async (): Promise<Connection> => {
    if (SQL.init) {
        await SQL.init();
        SQL.init = null;
    }

    let database: Buffer = null;
    let sqliteFilename = dataPath(createDBFilename());
    const alldb = natsort(await fg.async(dataPath(APP_NAME) + '*.sqlite3')).reverse();

    for (let filename of alldb) {
        try {
            database = await readFile(filename);
            if (database.length > 4096) {
                sqliteFilename = filename
                break;
            }
        } catch (err) {
            LOG_2ERRORS(filename, err);
        }
        database = null;
    }

    try {
        await Promise.all(alldb.map(f => f === sqliteFilename ? null : unlink(f)));
    } catch (err) {
        LOG_ERROR(err);
    }

    let connection = await getConnection(database, sqliteFilename);
    const migrations = await generateMigrations(connection);

    if (migrations.length > 0) {
        await connection.close();
        connection = await getConnection(database, sqliteFilename, migrations);
    }

    const driver = connection.driver as SqljsDriver;

    driver.constructor.prototype.export = function () {
        return this.databaseConnection;
    };

    connection.constructor.prototype.exportRaw = function (): Uint8Array {
        return (this.driver as SqljsDriver).databaseConnection.export();
    };

    repositories.forEach(r => r.provider.init(connection));

    for (let e of entities) {
        e.asyncProvider && await e.asyncProvider.init(connection);
    }

    if (database) {
        try {
            await connection.manager.query('VACUUM');
        } catch (err) {
            LOG_ERROR(err);
        }
    }
    return connection;
});

export const dbInit = provider.init;

@Provided(provider)
export abstract class DBConnection extends Connection {
    abstract exportRaw(): Uint8Array;
}