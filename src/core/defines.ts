declare const BROWSER: boolean;
declare const DEBUG: boolean;
declare const APP_NAME: string;
declare const APP_TITLE: string;
declare const PROJECT_SETTINGS_EXIST: boolean;
declare const v8debug: any;
declare const LOG_ERROR: (error) => void;
declare const LOG_2ERRORS: (...args: [any, any]) => void;
declare type FlashData = Readonly<{
    messages?: string[],
    warnings?: string[],
    errors?: string[]
}>

//browser only 
declare const EntitiesFactory: (mixins: any[], debounce: any) => any[];
declare const Paths: { [name: string]: string };
declare const UserInfo: { email: string, [name: string]: any };
declare const HasPty: boolean;
declare const LodableComponents: string[];
declare const Flash: FlashData;