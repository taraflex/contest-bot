'use strict';

const { basename, resolve } = require('path');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const { DefinePlugin, ContextReplacementPlugin } = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const autoprefixer = require('autoprefixer');
const { VueLoaderPlugin } = require('vue-loader');
const sentenceCase = require('sentence-case');
const { TsconfigPathsPlugin } = require('tsconfig-paths-webpack-plugin');
const VirtualModulePlugin = require('virtual-module-webpack-plugin');
const paramCase = require('param-case');
const { argv } = require('yargs');
const hogan = require('hogan.js');
const fs = require('fs');
const zopfli = require('@gfx/zopfli');
const PolyfillInjectorPlugin = require('webpack-polyfill-injector');
const pascalCase = require('pascal-case');
const merge = require('webpack-merge');
const AssetsPlugin = require('assets-webpack-plugin');

const DEV_SERVER = basename(require.main.filename) === 'webpack-dev-server.js';
//@ts-ignore
const DEV = !!(DEV_SERVER || argv.watch || (argv.mode && argv.mode.startsWith('dev')));

//@ts-ignore
const packageJson = require('../../package.json');
const APP_TITLE = sentenceCase(packageJson.name);
const extModules = new Set(Object.keys(packageJson.dependencies));

const cssLoader = {
    loader: 'css-loader',
    options: {
        sourceMap: DEV
    }
}
const sassLoader = {
    loader: 'sass-loader',
    options: {
        sourceMap: DEV
    }
}
const postcssLoader = DEV ? undefined : {
    loader: 'postcss-loader',
    options: {
        plugins: [
            autoprefixer({
                browsers: ['last 2 version', '> 1%', 'ie >= 11']
            })
        ],
        sourceMap: DEV
    }
}

const extensions = ['.js', '.ts', '.jsx', '.tsx', '.vue', '.pug'];

const u = (...a) => a.filter(e => !!e);

function readDir(...parts) {
    try {
        return fs.readdirSync(resolve(...parts));
    } catch (_) { }
    return [];
}

function getEntitiesFilenames(dir, noScanCore, ext = '.ts') {
    const files = (noScanCore ? [] : readDir(__dirname, dir))
        .concat(readDir(__dirname, '../' + dir))
        .filter(n => n.endsWith(ext))
        .map(n => basename(n, ext))
        .filter(n => pascalCase(n) === n);
    return Array.from(new Set(files));
}

const nameTemplate = (ext = 'js') => `[name].${ext}?h=[contenthash:7]`;

function config(BROWSER) {
    const BROWSER_PROD = !DEV && BROWSER;
    const styleLoader = DEV ? 'style-loader' : MiniCssExtractPlugin.loader;

    const vueComponents = BROWSER ? getEntitiesFilenames('frontend', true, '.vue') : [];

    return {
        target: BROWSER ? 'web' : 'node',
        devtool: DEV ? 'cheap-module-eval-source-map' : undefined,
        mode: DEV ? 'development' : 'production',
        externals: BROWSER ? [] : [function (context, request, callback) {
            //todo также сделать внешними module-folder/script.js
            if (extModules.has(request)) {
                return callback(null, 'commonjs ' + request);
            }
            callback();
        }],
        entry: BROWSER ? {
            index: DEV ? './src/core/frontend/index.ts' : `webpack-polyfill-injector?${JSON.stringify({
                modules: ['./src/core/frontend/index.ts']
            })}!`,
            common: './src/core/frontend/common.css',
        } : {
                index: './src/core/index.ts',
                launcher: './src/core/launcher.ts'
            },
        output: {
            devtoolModuleFilenameTemplate: '[absolute-resource-path]',
            path: resolve(__dirname, '../../', BROWSER ? 'build/static' : 'build'),
            publicPath: BROWSER ? './static/' : undefined,
            filename: nameTemplate(),
            chunkFilename: nameTemplate(),
        },
        resolve: {
            extensions,
            alias: {
                'mime-db$': 'mime-db/db.json',
                'vue$': 'vue/dist/vue.runtime.esm.js',
                'cli-highlight$': resolve(__dirname, './defines.ts'),
                'sql.js$': resolve(__dirname, './wasm-sql-dummy.js'),//хак, чтобы можно было задать wasm-sql.js как алиас sql.js, но не включать wasm-sql.js в сборку
                'typescript-ioc$': 'typescript-ioc/es6'
            },
            plugins: [new TsconfigPathsPlugin({ extensions })],
        },
        node: BROWSER ? {
            process: DEV,
            setImmediate: false,
        } : {
                __dirname: false
            },
        performance: { hints: false },
        optimization: {
            minimize: !DEV,
            minimizer: [
                new TerserPlugin({
                    terserOptions: {
                        ecma: BROWSER_PROD ? 5 : 8,
                        compress: {
                            //@ts-ignore
                            keep_classnames: true,
                            keep_fnames: true, //чтобы не сломать Function.name полифил
                            collapse_vars: false
                        },
                        output: {
                            beautify: !BROWSER,
                            comments: false
                        },
                        mangle: BROWSER_PROD ? {
                            reserved: [
                                ...vueComponents,
                                'x' //чтобы не сломать Function.name полифил
                            ]
                        } : false
                    }
                }),
                new OptimizeCssAssetsPlugin({
                    assetNameRegExp: /\.css(\?h=\w+)?$/,
                    cssProcessor: require('cssnano'),
                    //@ts-ignore
                    cssProcessorPluginOptions: {
                        preset: ['default', { discardComments: { removeAll: true }, zindex: false }],
                    }
                })
            ],
            splitChunks: {
                maxAsyncRequests: 1
            }
        },
        plugins: u(
            fs.existsSync(resolve(__dirname, '../App.ts')) ? 0 : new VirtualModulePlugin({
                moduleName: 'src/App.ts',
                contents: `
import { Singleton } from 'typescript-ioc';
@Singleton
export class App { 
    constructor() {} 
    async init() {}
}`}),
            //хак, чтобы можно было задать wasm-sql.js как алиас sql.js, но не включать wasm-sql.js в сборку
            BROWSER ? 0 : new VirtualModulePlugin({
                moduleName: 'src/core/wasm-sql-dummy.js',
                contents: 'module.exports = require("wasm-sql.js");'
            }),
            BROWSER ? new VirtualModulePlugin({
                moduleName: 'src/frontend/generated.ts',
                contents: hogan.compile(`
{{#files}}
//@ts-ignore
import {{{.}}} from '@frontend/{{{.}}}.vue';
{{/files}}
export default [{{#files}}{{{.}}},{{/files}}];
`).render({ files: vueComponents }),
            }) : 0,
            BROWSER ? 0 : new VirtualModulePlugin({
                moduleName: 'src/entities/generated.ts',
                contents: hogan.compile(`
import { getMetadataArgsStorage } from 'typeorm';
import { SingletonRepository } from '@ioc';
export const entities = []; 
export const repositories = [];
const storage = getMetadataArgsStorage();
{{#files}}
const { {{{.}}}, {{{.}}}Repository } = require('@entities/{{{.}}}.ts');
if({{{.}}} && storage.filterTables({{{.}}}).length > 0) {
    entities.push({{{.}}});
    if({{{.}}}Repository) {
        repositories.push(SingletonRepository({{{.}}})({{{.}}}Repository)); 
    }
}
{{/files}}
`).render({ files: getEntitiesFilenames('entities') }),
            }),
            BROWSER ? 0 : new VirtualModulePlugin({
                moduleName: 'src/subscribers/generated.ts',
                contents: hogan.compile(`
{{#files}}
import { {{{.}}} } from '@subscribers/{{{.}}}';
{{/files}}
export default [{{#files}}{{{.}}},{{/files}}];
`).render({ files: getEntitiesFilenames('subscribers') }),
            }),
            new ContextReplacementPlugin(
                /moment[/\\]locale$/,
                /ru|en-gb/
            ),
            new DefinePlugin({
                BROWSER: JSON.stringify(BROWSER),
                DEBUG: JSON.stringify(DEV),
                APP_NAME: "'" + paramCase(APP_TITLE) + "'",
                APP_TITLE: "'" + APP_TITLE + "'",
                PROJECT_SETTINGS_EXIST: fs.existsSync(resolve(__dirname, '../entities/Settings.ts')),
                'Number.MAX_SAFE_INTEGER': '9007199254740991',
                'Number.MIN_SAFE_INTEGER': '-9007199254740991',
                'process.env.ES6': "true",//for typescript-ioc
                'process.env.NODE_ENV': JSON.stringify(DEV ? 'development' : 'production'),
                ...(BROWSER ? { 'process.env.DEBUG': JSON.stringify(DEV) } : {})
            }),
            BROWSER ? new VueLoaderPlugin() : 0,
            BROWSER_PROD ? new PolyfillInjectorPlugin({
                singleFile: true,
                filename: '[name].js',//todo contenthash не работает
                polyfills: [
                    'Function.name',
                    'Symbol',
                    'Symbol.iterator',
                    'Promise',
                    'Set',
                    'Map',
                    'Object.assign',
                    'Object.keys',
                    'Object.values',
                    'Array.from',
                    'Array.prototype.find',
                    'Array.prototype.findIndex',
                    'String.prototype.endsWith',
                    'String.prototype.startsWith',
                    'String.prototype.trim',
                    'URL'
                ]
            }) : 0,
            BROWSER_PROD ? new MiniCssExtractPlugin({ filename: nameTemplate('css') }) : 0,
            BROWSER ? new AssetsPlugin({
                prettyPrint: true,
                fullPath: false,
                path: resolve(__dirname, '../../build'),
                //@ts-ignore
                entrypoints: true
            }) : 0,
            BROWSER && fs.existsSync(resolve(__dirname, '../../favicon.ico')) ? new CopyWebpackPlugin([
                {
                    from: './favicon.ico',
                    to: './'
                }
            ]) : 0,
            DEV ? 0 : new BundleAnalyzerPlugin({
                analyzerMode: 'static',
                openAnalyzer: false,
                reportFilename: 'build-report.htm'
            }),
            BROWSER_PROD ? new CompressionPlugin({
                filename: '[path].gz[query]',
                test: /\.(eot|svg|ttf|woff|js|css)(\?h=\w+)?$/,
                minRatio: 0.9,
                cache: true,
                compressionOptions: {
                    numiterations: 11,
                    blocksplitting: false
                },
                //@ts-ignore
                algorithm(input, compressionOptions, callback) {
                    return zopfli.gzip(input, compressionOptions, callback);
                }
            }) : 0
        ),
        module: {
            exprContextCritical: false,
            rules: [{
                test: /\.scss$/,
                use: u(styleLoader, cssLoader, postcssLoader, sassLoader)
            }, {
                test: /\.css$/,
                use: u(styleLoader, cssLoader, postcssLoader)
            }, {
                test: /\.tsx?$/,
                use: [{
                    loader: 'ts-loader',
                    options: {
                        compilerOptions: {
                            sourceMap: DEV,
                            target: BROWSER_PROD ? 'es5' : 'esnext',
                            noUnusedLocals: !DEV,
                            noUnusedParameters: !DEV
                        },
                        appendTsSuffixTo: [/\.vue$/],
                    }
                }, {
                    loader: 'preprocessor-loader',
                    options: {
                        macros: [
                            {
                                declaration: "LOG_ERROR(_0)",
                                definition: "console.error('(__FILE__:__LINE__)', _0)"
                            },
                            {
                                declaration: "LOG_2ERRORS(_1,_2)",
                                definition: "console.error('(__FILE__:__LINE__)',_1,_2)"
                            }
                        ]
                    }
                }],
                exclude: [/node_modules/]
            }, {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    loaders: {
                        ts: 'ts-loader',
                        js: 'ts-loader'
                    },
                    esModule: true
                }
            }, {
                test: /\.(png|jpg|gif|webp)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 4096,
                            name: '[name].[md5:hash:base58:7].[ext]',
                            publicPath: DEV ? 'static/assets/' : 'assets/',
                            outputPath: 'assets/'
                        }
                    }
                ]
            }, {
                test: /\.(eot|svg|ttf|woff|woff2)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 8192,
                            name: '[md5:hash:base58:7].[ext]',
                            publicPath: DEV ? 'static/assets/' : 'assets/',
                            outputPath: 'assets/'
                        }
                    }
                ]
            }, {
                test: /\.pug$/,
                loader: 'pug-loader',
                options: {
                    pretty: DEV
                }
            }, {
                test: /\.mustache(\.conf)?$/,
                loader: 'mustache-loader',
                options: {
                    tiny: !DEV
                }
            }],
        },
        resolveLoader: {
            modules: ['node_modules', resolve(__dirname, 'loaders'), resolve(__dirname, '../loaders')]
        },
        stats: {
            children: false,
            warningsFilter: /mongodb|mssql|mysql|mysql2|oracledb|pg|pg-native|redis|pg-query-stream|sqlite3|react-native/
        }
    }
}

module.exports = function (webConfig, nodeConfig) {
    return argv.target == 'web' || argv.target == 'node' ? config(argv.target == 'web') : [
        //@ts-ignore
        merge(config(true), webConfig),
        //@ts-ignore
        merge(config(false), nodeConfig),
    ];
}