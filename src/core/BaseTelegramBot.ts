import { EventEmitter } from 'events';
import { createReadStream } from 'fs';
import { RouterContext } from 'koa-router';
import * as TelegramBot from 'node-telegram-bot-api';
import * as rawBody from 'raw-body';
import { Options } from 'request';
import { Inject } from 'typescript-ioc';
import { IDisposable } from 'xterm';

import { Settings } from '@entities/Settings';
import { MAX_PAYLOAD_SIZE } from '@middlewares/body-parse-msgpack';
import { BaseRequestOpts, promiseRequest } from '@utils/cancelable-request';
import { HOOKS } from '@utils/config';
import notEmptyFile from '@utils/not-empty-file';
import { rndString } from '@utils/rnd';
import { post } from '@utils/routes-helpers';
import { SSL_CERT_LOCATION } from '@utils/ssl-utils';
import { getBotInfo } from '@utils/tg-utils';

const camelCase = require('camel-case');
const paramCase = require('param-case');
const TELEGRAM_ERRORS = new Set([400, 403, 404]);//todo may be remove 400 code or extendable filter

export function isCantWriteError(err: any) {
    return err && err.code === 'ETELEGRAM' &&
        err.response && err.response.body && TELEGRAM_ERRORS.has(err.response.body.error_code);
}

const hasSertificate = notEmptyFile(SSL_CERT_LOCATION);

export abstract class BaseTelegramBot implements IDisposable {

    public bot: TelegramBot = null;
    protected lastWebHookToken: string;
    protected readonly allowedUpdates = new Set<keyof TelegramBot.Update>();
    protected lastRoot = '';
    public botInfo: TelegramBot.User;
    @Inject protected readonly settings: Settings;

    async dispose() {
        if (this.bot) {
            try {
                await promiseRequest('GET', `https://api.telegram.org/bot${this.token}/deleteWebhook`);
            } catch (err) {
                LOG_2ERRORS(this.token, err);
            }
            (this.bot as EventEmitter).removeAllListeners();
            this.bot = null;
        }
    }

    async update(token: string) {
        if (this.lastRoot !== this.settings.rootUrl) {
            await this.dispose();
        }
        if (token && token === this.token) {
            return true;
        }
        const botInfo = await getBotInfo(token);

        await this.dispose();

        const { body } = await promiseRequest('POST', {
            url: `https://api.telegram.org/bot${token}/setWebhook`,
            formData: {
                ...(hasSertificate ? { certificate: createReadStream(SSL_CERT_LOCATION) } : {}),
                url: new URL(this.settings.rootUrl).origin.replace(/^http:/i, 'https:') + `${HOOKS}/process-${paramCase(this.constructor.name)}/` + (this.lastWebHookToken = await rndString()),
                allowed_updates: JSON.stringify(Array.from(this.allowedUpdates))
            }
        });
        if (JSON.parse(body).result !== true) {
            throw body.toString();
        }

        this.bot = new TelegramBot(token, { request: <Options>BaseRequestOpts });

        this.allowedUpdates.forEach(update => {
            this.bot.on(<any>update, this[camelCase('on_' + update)].bind(this));
        });

        this.botInfo = botInfo;

        this.lastRoot = this.settings.rootUrl;

        return true;
    }

    @post({ path: function () { return `/process-${paramCase(this.constructor.name)}/:token` } })
    async processTelegramMessages(ctx: RouterContext) {
        if (ctx.params.token === this.lastWebHookToken && this.bot) {
            const updates = JSON.parse(await rawBody(ctx.req, { limit: MAX_PAYLOAD_SIZE, encoding: 'utf-8' }));
            this.bot.processUpdate(updates);
        }
        ctx.status = 200;
        ctx.body = '';
    }

    get token() {
        //@ts-ignore
        return (this.bot && this.bot.token) || '';
    }
}