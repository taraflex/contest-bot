import 'reflect-metadata';

import { ColumnOptions, getMetadataArgsStorage } from 'typeorm';

import {
    Action, AdditionalProperties, ArrayValidator, DisplayInfo, EntityClass, has, NumberValidator,
    RTTIItem, RTTIItemState, Validator, ValidatorType
} from '@crud/types';
import isValidJsVar from '@utils/is-valid-js-var';
import { compile } from '@utils/validator-helpers';

function nullPrune(o: RTTIItem) {
    for (let k in o) {
        if (o[k] == null) {
            delete o[k];
        }
    }
    return o;
}

function getFieldSchema(target: EntityClass, options: ColumnOptions, propertyName: string): RTTIItem {
    const { nullable, unsigned, array, zerofill, primary } = options;
    let type: ValidatorType;
    try {
        type = <any>(options.type as Function).name.toLowerCase();
    } catch (_) {
        type = <any>options.type.toString();
    }

    let vo = target.__validatorInfo && target.__validatorInfo[propertyName];

    const _enum = options.enum || (vo && (vo as ArrayValidator).enum);

    let enumPairs: any[][] = undefined;
    let enumValues: any[] = undefined;

    const generated = getMetadataArgsStorage().generations.some(v => v.target === target && v.propertyName === propertyName);

    if (Array.isArray(_enum)) {
        enumValues = _enum;
        enumPairs = enumValues.map(v => [v, v]);
    } else if (_enum) {
        const keys = new Set(Object.keys(_enum));
        while (keys.size > 0) {
            const { value } = keys.values().next();
            let k: string = value;
            let v = _enum[k];
            if (
                !isValidJsVar(k) ||
                (_enum[v] === k && isValidJsVar(v) && v.toUpperCase() == v && k.toUpperCase() !== k) //v constant case and k not
            ) {
                keys.delete(k);
                continue;
            }

            if (!Array.isArray(enumPairs)) {
                enumPairs = [];
                enumValues = [];
            }

            keys.delete(v.toString());
            keys.delete(k);
            enumPairs.push([k, v]);
            enumValues.push(v);
        }
    }

    let integer = undefined;

    if (type.includes('int')) {
        type = 'number';
        integer = true;
    } else if (type.includes('date') || type.includes('time')) {
        type = 'date';
    } else if (type.includes('string') || type.includes('char') || type.includes('clob') || type.includes('text')) {
        type = 'string';
    } else if (type.includes('bool')) {
        type = 'boolean';
    } else if (type.includes('blob') || type.includes('binary')) {
        throw 'Validator not implemented';
    } else if (<any>type === 'simple-array' || <any>type === 'simple-json' || type === 'array' || array) {
        type = 'array';
    } else if (type === 'enum') {

    } else {
        type = 'number';
    }

    if (!Array.isArray(enumValues) || enumValues.length < 1) {
        enumValues = enumPairs = undefined;
    }

    let state = vo ? vo.state | 0 : 0;

    if (type === 'array' || type === 'enum' || enumValues) {
        if (//todo check if allow multiple
            (state & RTTIItemState.ALLOW_CREATE) === RTTIItemState.ALLOW_CREATE ||
            !enumValues ||
            (vo && (vo as ArrayValidator).min > 1)
        ) {
            enumValues = null;
            type = 'array';
        } else {
            type = 'enum';
        }
        if (vo) {
            vo = { ...vo };
            delete (vo as ArrayValidator).enum;
        }
    }

    const isNumber = type === 'number';

    const o: RTTIItem = {
        type,
        enumPairs,
        enum: enumValues,
        //@ts-ignore
        values: enumValues,
        integer,
        min: isNumber && (unsigned || zerofill || (vo && (vo as NumberValidator).positive)) ? 0 : null,
        max: isNumber && vo && (vo as NumberValidator).negative ? 0 : null,
        optional: !generated && (nullable || (options.default != null)),
        default: zerofill && isNumber && options.default == null ? 0 : options.default,
        state
    }
    const r = nullPrune(vo ? Object.assign(o, vo) : o);
    if (r.type === 'link') {
        r.state = r.state | RTTIItemState.READONLY;
    }
    if (primary) {
        r.state = r.state | RTTIItemState.PRIMARY | (generated ? RTTIItemState.HIDDEN : 0);
    }
    return r;
}

function validate(o) {
    const r = this(o);
    if (r !== true) {
        throw r;
    }
}

function* getColumns(entity: EntityClass) {
    if (entity && entity !== Function.prototype) {
        const selfColumns = getMetadataArgsStorage().filterColumns(entity);
        yield* getColumns(Object.getPrototypeOf(entity));
        for (let column of selfColumns) {
            yield column;
        }
    }
}

export function Access(allowedFor?: { [_ in Action]?: number }, displayInfo?: DisplayInfo, hooks?: { [_ in Action]?: string }) {
    return (entity: EntityClass) => {
        const vInsert = Object.create(null);
        const vUpdate = Object.create(null);

        for (let { propertyName, options } of getColumns(entity)) {
            const schema = getFieldSchema(entity, options, propertyName);
            if (!has(schema, RTTIItemState.READONLY)) {
                vInsert[propertyName] = schema;
            }
            vUpdate[propertyName] = schema;
        }

        allowedFor = allowedFor || Object.create(null);
        if (allowedFor._ !== +allowedFor._) {
            allowedFor._ = -1;
        }
        Object.freeze(allowedFor);

        Object.defineProperty(entity, 'checkAccess', {
            value(action: Action, user: { role: number }) {
                if (!user) {
                    throw 401;
                }
                let t = allowedFor[action];
                if (t == null) {
                    t = allowedFor._;
                }
                if ((t & user.role) !== t) {
                    throw 403;
                }
                return true;
            }
        });

        for (const _ in vInsert) {
            Object.defineProperty(entity, 'insertValidate', {
                value: validate.bind(compile(vInsert))
            });
            break;
        }

        for (const _ in vUpdate) {
            Object.defineProperty(entity, 'updateValidate', {
                value: validate.bind(compile(vUpdate))
            });
            break;
        }

        displayInfo || (displayInfo = Object.create(null));
        if (!displayInfo.display) {
            displayInfo.display = entity.asyncProvider ? 'single' : 'table';
        }

        Object.defineProperty(entity, 'rtti', {
            value: Object.freeze({
                props: Object.freeze(vUpdate),
                displayInfo: Object.freeze(displayInfo)
            })
        });

        Object.defineProperty(entity, 'hooks', {
            value: hooks || Object.create(null)
        });

        delete entity.__validatorInfo;

        return entity;
    }
}

export function v(validatorProps: Validator & AdditionalProperties) {
    return ({ constructor }: { constructor: Function }, prop: string | symbol): void => {
        ((constructor as EntityClass).__validatorInfo || ((constructor as EntityClass).__validatorInfo = Object.create(null)))[prop] = Object.freeze(validatorProps);
    };
}