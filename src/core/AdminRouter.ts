import * as cp from 'fs-cp';
import { Context } from 'koa';
import * as compress from 'koa-compress';
import * as helmet from 'koa-helmet';
import * as Router from 'koa-router';
import * as pluralize from 'pluralize';
import * as toSource from 'tosource';
import { Container, Inject, Singleton } from 'typescript-ioc';

import { Action, ValidatorType } from '@crud/types';
import { entities } from '@entities';
import { Settings } from '@entities/Settings';
import { User } from '@entities/User';
import checkAuth from '@middlewares/check-auth';
import flash from '@middlewares/flash';
import installed from '@middlewares/installed';
import noStore from '@middlewares/no-store';
import normalizeTralingSlashes from '@middlewares/normalize-traling-slashes';
import pug from '@middlewares/pug';
import redirectAfterError from '@middlewares/redirect-after-error';
import smartRedirect from '@middlewares/smart-redirect';
import dataPath from '@utils/data-path';
import isValidJsVar from '@utils/is-valid-js-var';
import * as PtyFactory from '@utils/pty-factory';
import { applyRoutes } from '@utils/routes-helpers';
import rtti2vue from '@utils/rtti-to-vue-component';

import { AuthRegister } from './AuthRegister';
import { createDBFilename, DBConnection } from './DBConnection';
import { MainRouter } from './MainRouter';
import { InstallRoutes } from './routes/install';
import { LoginRoutes } from './routes/login';
import { ResetPassRoutes } from './routes/resetpass';

const componentsTemplate = require('@templates/components/index.mustache');
const paramCase = require('param-case');

async function restart(ctx: Context) {
    ctx.status = 201;
    ctx.type = 'json';
    ctx.body = '{"success":"ok"}';
    ctx.res.once('finish', () => process.exit(2));
}

@Singleton
export class AdminRouter extends Router {

    constructor(
        @Inject dbconnection: DBConnection,
        @Inject settings: Settings,
        @Inject installRoutes: InstallRoutes,
        @Inject loginRoutes: LoginRoutes,
        @Inject resetPassRoutes: ResetPassRoutes,
        @Inject { session, passport, passportSession }: AuthRegister
    ) {
        super();
        this
            .use(
                helmet({ hsts: false, referrerPolicy: true }),
                smartRedirect, //redirect to named route 
                normalizeTralingSlashes,
                redirectAfterError('login'),
                session,
                passport,
                passportSession,
                flash, //flash messages
                pug() //render pug template
            )
            .get('logout', '/logout', ctx => {
                ctx.logout();
                ctx.namedRedirect('login');
            });
        applyRoutes(this, installRoutes);
        if (!settings.installed) {
            this.use(installed(settings));
        }
        applyRoutes(this, loginRoutes);
        applyRoutes(this, resetPassRoutes);

        const componentsCache = {};

        this.use(checkAuth);

        this.get('root', '/', (ctx: Context) => {
            const user = ctx.state.user as User;
            if (!componentsCache[user.role]) {
                const components = [];
                const lc = new Set<ValidatorType>();
                for (let entity of entities) {
                    let { name, rtti, checkAccess, hooks } = entity;
                    name = pluralize(name);

                    const can = Object.create(null);
                    for (let action of ['GET', 'PATCH', 'PUT', 'DELETE']) {
                        try {
                            can[action] = checkAccess(action as Action, ctx.state.user);
                        } catch (_) { }
                    }
                    if (can.GET && rtti.displayInfo.display) {
                        can.CHANGE = can.PUT || can.PATCH;
                        const render = rtti2vue(rtti, can, ctx);
                        const remote = Object.values(rtti.props)
                            .filter(v => v.remote)
                            .map(v => {
                                const i = v.remote.lastIndexOf('.');
                                const c = v.remote.slice(0, i);
                                const method = v.remote.slice(i + 1);
                                if (!isValidJsVar(c) || !isValidJsVar(method) || method == v.remote) {
                                    throw `Invalid property ${toSource(v)}`;
                                }
                                return { c, method };
                            });

                        for (const p in rtti.props) {
                            lc.add(rtti.props[p].type);
                        }

                        components.push({
                            name,
                            sortable: !!(can.PATCH && rtti.displayInfo.sortable && rtti.displayInfo.display !== 'single'),
                            order: rtti.displayInfo.order | 0,
                            icon: rtti.displayInfo.icon,
                            render,
                            can: toSource(can, null, false),
                            rtti: toSource(rtti, null, false),
                            hooks: toSource(hooks, null, false),
                            endPoint: paramCase(name),
                            remote
                        });
                    }
                }
                componentsCache[user.role] = {
                    components,
                    paths: (Container.get(MainRouter) as Router).stack.filter(l => l.name).map(l => ({
                        name: l.name,
                        url: new URL(ctx.resolve(l.name)).pathname
                    })),
                    lodableComponents: Array.from(lc)
                };
            }

            ctx.pug('index', {
                entry: 'index',
                inlineScript: componentsTemplate({
                    ...componentsCache[user.role],
                    email: user.email,
                    flash: toSource(ctx.flash, null, false),
                    hasPty: PtyFactory.avalible
                })
            }, true);
        });

        this.get('backup', '/backup', noStore, compress(), (ctx: Context) => {
            ctx.attachment(createDBFilename());
            ctx.body = Buffer.from(dbconnection.exportRaw().buffer as ArrayBuffer);
        });

        //put вместо get/post потому что возможен csrf
        this.put('restore', '/restore', async (ctx: Context) => {
            await cp(ctx.req, dataPath(createDBFilename()));
            await restart(ctx);
        });

        this.put('restart', '/restart', restart);
    }
}