const services = Object.freeze({
    'mail.ru': Object.freeze({ title: 'почту Mail.Ru', url: 'https://e.mail.ru/' }),
    'bk.ru': Object.freeze({ title: 'почту Mail.Ru (bk.ru)', url: 'https://e.mail.ru/' }),
    'list.ru': Object.freeze({ title: 'почту Mail.Ru (list.ru)', url: 'https://e.mail.ru/' }),
    'inbox.ru': Object.freeze({ title: 'почту Mail.Ru (inbox.ru)', url: 'https://e.mail.ru/' }),
    'yandex.ru': Object.freeze({ title: 'Яндекс.Почта', url: 'https://mail.yandex.ru/' }),
    'ya.ru': Object.freeze({ title: 'Яндекс.Почта', url: 'https://mail.yandex.ru/' }),
    'yandex.ua': Object.freeze({ title: 'Яндекс.Почта', url: 'https://mail.yandex.ua/' }),
    'yandex.by': Object.freeze({ title: 'Яндекс.Почта', url: 'https://mail.yandex.by/' }),
    'yandex.kz': Object.freeze({ title: 'Яндекс.Почта', url: 'https://mail.yandex.kz/' }),
    'yandex.com': Object.freeze({ title: 'Yandex.Mail', url: 'https://mail.yandex.com/' }),
    'gmail.com': Object.freeze({ title: 'Gmail', url: 'https://mail.google.com/' }),
    'googlemail.com': Object.freeze({ title: 'Gmail', url: 'https://mail.google.com/' }),
    'outlook.com': Object.freeze({ title: 'Outlook.com', url: 'https://mail.live.com/' }),
    'hotmail.com': Object.freeze({ title: 'Outlook.com (Hotmail)', url: 'https://mail.live.com/' }),
    'live.ru': Object.freeze({ title: 'Outlook.com (live.ru)', url: 'https://mail.live.com/' }),
    'live.com': Object.freeze({ title: 'Outlook.com (live.com)', url: 'https://mail.live.com/' }),
    'me.com': Object.freeze({ title: 'iCloud Mail', url: 'https://www.icloud.com/' }),
    'icloud.com': Object.freeze({ title: 'iCloud Mail', url: 'https://www.icloud.com/' }),
    'rambler.ru': Object.freeze({ title: 'Рамблер/почта', url: 'https://mail.rambler.ru/' }),
    'yahoo.com': Object.freeze({ title: 'Yahoo! Mail', url: 'https://mail.yahoo.com/' }),
    'ukr.net': Object.freeze({ title: 'почту ukr.net', url: 'https://mail.ukr.net/' }),
    'i.ua': Object.freeze({ title: 'почту I.UA', url: 'http://mail.i.ua/' }),
    'bigmir.net': Object.freeze({ title: 'почту Bigmir.net', url: 'http://mail.bigmir.net/' }),
    'tut.by': Object.freeze({ title: 'почту tut.by', url: 'https://mail.tut.by/' }),
    'inbox.lv': Object.freeze({ title: 'Inbox.lv', url: 'https://www.inbox.lv/' }),
    'mail.kz': Object.freeze({ title: 'почту mail.kz', url: 'http://mail.kz/' }),
});

export default (email: string): Readonly<{ title: string, url: string }> | null => {
    const m = email.split('@');
    const domain = m && m.length > 1 ? m[m.length - 1] : null;
    return services[domain] || '';
}