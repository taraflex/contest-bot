import { Context } from 'koa';
import { encode } from 'msgpack-lite';
import { IPty } from 'node-pty';
import * as ServerWebSoket from 'ws';

import { User } from '@entities/User';
import msgpackOptions from '@utils/msgpack-options';
import * as PtyFactory from '@utils/pty-factory';
import { Answer, Request, RPC } from '@utils/RPC';
import stringify from '@utils/stringify';

export class ChainWebSoket extends ServerWebSoket {
    _chain?: ServerRPC;
}

function onWSMessage(this: ChainWebSoket, message) {
    const { _chain } = this;
    if (_chain) {
        _chain.lastActive = Date.now();
        _chain.process(message).catch(err => LOG_ERROR(err));
    }
}

function onWSClose(this: ChainWebSoket) {
    try {
        const { _chain } = this;
        _chain && _chain.dispose();
    } catch (err) {
        LOG_ERROR(err);
    }
}

function onWSPong(this: ChainWebSoket) {
    const { _chain } = this;
    _chain && (_chain.lastActive = Date.now());
}

//todo clear chain when remove user
const сhains = new Map<number, Map<number, ServerRPC>>();

export function remote<I = any>(key: string, ctx: Context, timeout?: number) {
    return сhains.get(ctx.state.user.id).get(parseInt(ctx.headers['x-tab'])).remote<I>(key, timeout);
}

export class ServerRPC extends RPC {
    lastActive: number = 0;

    constructor(protected socket: ChainWebSoket, protected tab: number, protected user: User, terminalSize?) {
        super();

        if (tab !== +tab || !tab) {
            throw 'Invalid websocket tab: ' + stringify(tab);
        }

        socket._chain = this;
        onWSPong.apply(socket);

        let userChains = сhains.get(user.id);
        if (!userChains) {
            сhains.set(user.id, userChains = new Map());
        }
        userChains.set(tab, this);

        socket.on('close', onWSClose);
        socket.on('pong', onWSPong);
        socket.on('message', onWSMessage);

        if (terminalSize) {
            this.enableTerminal(terminalSize);
        }
    }

    send(data: Answer | Request) {
        //пока функция выполняется соединение может успеть разорваться, поэтому получим новый chain по id вкладки браузера
        return сhains.get(this.user.id).get(this.tab).socket.send(encode(data, msgpackOptions));
    }

    enableTerminal(terminalSize: { cols: number, rows: number }) {
        if (!this.term) {
            const term = PtyFactory.create(terminalSize);
            if (term) {
                this.term = term;
                this.socket.send(String.fromCharCode(27) + 'c');
                term.on('data', data => {
                    try {
                        this.socket.send(data);
                    } catch (err) {
                        LOG_ERROR(err);
                    }
                });
                //todo auto restart
                //term.on('exit', () => {});
            }
        }
    }

    protected disableTerminal() {
        if (this.term) {
            (this.term as IPty).kill();
            this.term = undefined;
        }
    }

    dispose() {
        this.disableTerminal();
        if (this.socket) {
            const c = сhains.get(this.user.id);
            if (c && c.get(this.tab) === this) {
                c.delete(this.tab);
            }
            this.socket._chain = undefined;
            if (
                this.socket.readyState != ServerWebSoket.CLOSING &&
                this.socket.readyState != ServerWebSoket.CLOSED
            ) {
                this.socket.close();
            }
            this.socket = undefined;
        }
    }

    checkActive() {
        if (Date.now() - this.lastActive > 45000) {
            this.dispose();
        } else {
            this.socket.ping();
        }
    }
}