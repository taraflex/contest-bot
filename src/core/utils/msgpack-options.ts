import { createCodec, DecoderOptions, EncoderOptions } from 'msgpack-lite';

export default {
    codec: createCodec({
        preset: true,
        safe: false,
        useraw: false,
        int64: false,
        binarraybuffer: true,
        uint8array: BROWSER,
        usemap: false
    })
} as EncoderOptions | DecoderOptions;