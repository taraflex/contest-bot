import { exec } from 'child-process-promise';
import { readFile } from 'fs-extra';
import { validateSSL } from 'ssl-validator';

export const SSL_CERT_LOCATION = '/home/localhost.cert';
export const SSL_KEY_LOCATION = '/home/localhost.key';

export async function isValidSslCert(ip: string) {
    try {
        await validateSSL(await readFile(SSL_CERT_LOCATION), {
            key: await readFile(SSL_KEY_LOCATION),
            domain: ip
        });
    } catch (_) {
        return false;
    }
    return true;
}

export function genSslCert(ip: string): Promise<void> {
    //todo escape path
    return exec(`openssl req -newkey rsa:1024 -sha256 -nodes -keyout ${SSL_KEY_LOCATION} -x509 -days 3650 -out ${SSL_CERT_LOCATION} -subj "/C=US/ST=New York/L=Brooklyn/O=Example Company/CN=${ip}"`)
}