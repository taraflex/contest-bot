import * as FV from 'fastest-validator';
import * as checkArray from 'fastest-validator/lib/rules/array';
import * as checkString from 'fastest-validator/lib/rules/string';

import { ValidatorType } from '@crud/types';

export function typeDefault(type: ValidatorType) {
    switch (type) {
        case 'enum':
        case 'number':
            return 0;
        case 'string':
        case 'wysiwyg':
        case 'md':
        case 'link':
            return '';
        case 'boolean':
            return false;
        case 'files':
        case 'array':
            return [];
        case 'date':
            return new Date();
        case 'any':
        case 'object':
            return Object.create(null);
    }
    return null;
}

const v = new FV();
v.add('wysiwyg', checkString);
v.add('link', checkString);
v.add('md', checkString);
v.add('files', checkArray);

export const compile = v.compile.bind(v);