import { createCipheriv, createDecipheriv } from 'crypto';
import * as rndBytes from 'random-bytes-seed';

import { STRING_SEED } from '@utils/rnd';

const rndGen = rndBytes(STRING_SEED);
const chachakey: Buffer = rndGen(256 / 8);
const chachaiv: Buffer = rndGen(96 / 8);

export function encrypt(data: Uint8Array): string {
    const cipher = createCipheriv('chacha20-poly1305', chachakey, chachaiv);
    return cipher.update(data).toString('base64');
}

export function decrypt(s: string) {
    const data = Buffer.from(s, 'base64');
    const decipher = createDecipheriv('chacha20-poly1305', chachakey, chachaiv);
    return decipher.update(data);
}