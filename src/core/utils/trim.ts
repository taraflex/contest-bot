import stringify from '@utils/stringify';

export default function (s?: string): string {
    return s ? stringify(s).trim() : '';
}