import { Context } from 'koa';
import { compileTemplate } from 'pug';
import { compile } from 'vue-template-compiler';
import * as transpile from 'vue-template-es2015-compiler';

import {
    Action, DisplayInfoTypes, has, RowState, RTTI, RTTIItem, RTTIItemState
} from '@crud/types';

const templates: { [x in DisplayInfoTypes]: compileTemplate } = {
    single: require('@templates/components/single.pug'),
    table: require('@templates/components/table.pug')
}

function isExtendableArrayField(v) { return has(v, RTTIItemState.ALLOW_CREATE) }
function isVisibleField(v) { return !has(v, RTTIItemState.HIDDEN) }
function isEditableField(v) { return !has(v, RTTIItemState.READONLY) }
function isNormalField(v) { return !has(v, RTTIItemState.HIDDEN) && !has(v, RTTIItemState.FULLWIDTH) }
function isFullwidthField(v) { return !has(v, RTTIItemState.HIDDEN) && has(v, RTTIItemState.FULLWIDTH) }
function findPrimaryIndex(props: { [_: string]: RTTIItem }) {
    let i = 0;
    for (let k in props) {
        if (has(props[k], RTTIItemState.PRIMARY)) {
            return i;
        }
        ++i;
    }
    throw new Error("Can't find primary field.");
}

function toFunction(code) { return `(function(){${code}})`; }

export default ({ props, displayInfo }: RTTI, can: { [action in Action]: boolean }, ctx: Context) => {
    const vueTemplate = templates[displayInfo.display]({
        props,
        EDIT: RowState.EDIT,
        NONE: RowState.NONE,
        REMOVE: RowState.REMOVE,
        SAVE: RowState.SAVE,
        can,
        ctx,
        isExtendableArrayField,
        findPrimaryIndex,
        isVisibleField,
        isEditableField,
        isNormalField,
        isFullwidthField
    });

    const { render, staticRenderFns } = compile(vueTemplate, { preserveWhitespace: false });
    return transpile(`(function(){
return {staticRenderFns:[${staticRenderFns.map(toFunction)}],render:${toFunction(render)}};
})()`);
}