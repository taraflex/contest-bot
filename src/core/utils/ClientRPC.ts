import { encode } from 'msgpack-lite';
import { IDisposable, Terminal } from 'xterm';

import { notify } from '@frontend/error-handler-mixin';
import { TAB_ID } from '@utils/axios-smart';
import msgpackOptions from '@utils/msgpack-options';
import { Answer, Request, RPC } from '@utils/RPC';

declare module 'vue/types/vue' {
    interface Vue {
        $rpc: ClientRPC;
    }
}
// todo waitConnect before any remote call
export class ClientRPC extends RPC {
    ws: WebSocket;
    lastTermListener: IDisposable;
    protected readonly url = location.origin.replace(/^http/i, 'ws') + location.pathname + 'ws/';
    constructor() {
        super();
        this.restart();
    }
    get isConnected() {
        return this.ws && this.ws.readyState === WebSocket.OPEN;
    }
    protected readonly restart = () => {
        this.dispose();
        if (this.term) {
            const { cols, rows } = this.term as Terminal;
            this.ws = new WebSocket(this.url + `?cols=${cols}&rows=${rows}&tab=${TAB_ID}`);
            this.ws.onopen = this.onopen;
        } else {
            this.ws = new WebSocket(this.url + '?tab=' + TAB_ID);
        }
        this.ws.binaryType = 'arraybuffer';
        this.ws.onerror = this.ws.onclose = this.restart;
        this.ws.onmessage = this.onmessage;
    }
    async waitConnect() {
        if (this.ws && this.ws.readyState !== WebSocket.OPEN) {
            await new Promise((resolve, reject) => {
                this.ws.addEventListener('open', resolve);
                this.ws.addEventListener('error', reject);
                this.ws.addEventListener('close', reject);
            });
        }
    }
    async enableTerminal(term: Terminal) {
        if (this.ws) {
            await this.waitConnect();
            const { searchParams } = new URL(this.ws.url);
            if (!searchParams.has('cols') || !searchParams.has('rows')) {
                await this.remote<RPC>('RPC', 30).enableTerminal({ cols: term.cols, rows: term.rows });
            }
            this.lastTermListener && this.lastTermListener.dispose();
            this.lastTermListener = term.addDisposableListener('data', data => {
                this.ws.send(data);
            });
        }
        this.term = term;
    }
    protected readonly onopen = () => {
        if (this.term) {
            this.enableTerminal(this.term as Terminal).catch(notify);
        }
    }
    protected readonly onmessage = (message: MessageEvent) => {
        this.process(message.data).catch(notify);
    }
    send(data: Answer | Request) {
        this.ws.send(encode(data, msgpackOptions));
    }
    dispose() {
        this.lastTermListener && this.lastTermListener.dispose();
        if (this.ws) {
            this.ws.onclose = undefined;
            this.ws.onerror = undefined;
            this.ws.onopen = undefined;
            this.ws.onmessage = undefined;
            this.ws.close();
            this.ws = null;
        }
    }
}