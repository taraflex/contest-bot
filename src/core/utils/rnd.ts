import { randomBytes } from 'crypto';
import { machineIdSync } from 'node-machine-id';
import * as rndSeed from 'random-bytes-seed';
import { create } from 'random-seed';
import { promisify } from 'util';

import { PORT } from './config';

const encode32 = require('base32-encode');
const rndBytes = promisify(randomBytes);
export const STRING_SEED = APP_NAME + PORT + machineIdSync(true);

export async function rndString(): Promise<string> {
    return encode32((await rndBytes(~~(Math.random() * 7) + 23)).buffer as ArrayBuffer, 'Crockford').toLowerCase();
}

export function seedRndString(seed: string = STRING_SEED): string {
    const b: Buffer = rndSeed(seed)(create(seed).intBetween(23, 23 + 7));
    return encode32(b.buffer as ArrayBuffer, 'Crockford').toLowerCase();
}