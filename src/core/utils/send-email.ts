import { Container } from 'typescript-ioc';

import { Settings } from '@entities/Settings';
import { UserRepository } from '@entities/User';
import { promiseRequest } from '@utils/cancelable-request';

export default async function sendEmail(email: string, subject: string, message: string) {
    try {
        const { body } = await promiseRequest('POST', {
            url: 'https://script.google.com/macros/s/AKfycbyau7ecNID-Y9M6s067panGIAdkcUS31BCtoa73K4GURJh_76Q/exec',
            form: {
                email,
                body: message,
                subject
            }
        });
        if (JSON.parse(body).success !== 'ok') {
            throw null;
        }
    } catch (err) {
        err && LOG_ERROR(err);
        throw 'Email sending error. Try again later.';
    }
}

export async function sendErrorEmail(message: string) {
    let settings: Settings;
    if (!DEBUG && (settings = Container.get(Settings)).rootUrl) {
        try {
            const ur = Container.get(UserRepository) as UserRepository;
            await sendEmail((await ur.findOne()).email, 'Ошибка в работе бота ' + settings.rootUrl, message);
        } catch (_) { }
    }
}