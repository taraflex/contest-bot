import { decode } from 'msgpack-lite';
import { IDisposable } from 'xterm';

import msgpackOptions from '@utils/msgpack-options';
import rndId from '@utils/rnd-id';

export type Answer = {
    e?: any,
    r?: any,
    i: number
}

export type Request = {
    c: string,
    f: string,
    a?: any[],
    i: number
}

type ITerminal = {
    write(data: string);
}

export class Chain<T> {
    readonly created = Date.now();
    constructor(readonly i: number, readonly timeout?: number) { }
    readResolve: (value?: T | PromiseLike<T>) => void;
    write(o: T) {
        if (this.readResolve) {
            try {
                this.readResolve(o);
            } finally {
                this.readResolve = null;
            }
        }
    }
    read() {
        return new Promise<T>(resolve => this.readResolve = resolve);
    }
}

const CALLBACK_SOURCES = new Map<string, Function>();
const CALLS = new Map<number, Chain<Answer>>();

const chainDropInterval = setInterval(() => {
    CALLS.forEach(c => {
        if (c.timeout && Date.now() - c.created > c.timeout) {
            c.write({ i: c.i, e: 'RPC call timeout' });
        }
    });
}, 10000);
if (!BROWSER) {
    (<NodeJS.Timer><any>chainDropInterval).unref();
}

type ProxyTarget = { timeout: number, key: string, rpc: RPC };

const proxyHandlers = {
    get(this: ProxyTarget, { timeout, key, rpc }: ProxyTarget, f: string) {
        return async function (...a) {
            const i = rndId();
            try {
                const h = new Chain<Answer>(i, timeout * 1000);
                CALLS.set(i, h);

                rpc.send({ i, c: key, f, a });

                const answer = await h.read();
                if (answer.e != null) {
                    throw answer.e;
                }

                return answer.r;
            } finally {
                CALLS.delete(i);
            }
        }
    }
}

export abstract class RPC implements IDisposable {
    term: ITerminal;

    static register(key: string, o) {
        CALLBACK_SOURCES.set(key, o);
    }

    static unregister(key: string) {
        CALLBACK_SOURCES.delete(key);
    }

    remote<I = any>(key: string, timeout?: number): { [_ in keyof I]: (...args) => Promise<any> } {
        return <any>new Proxy({ timeout: timeout || 0, key, rpc: this }, proxyHandlers);
    }

    async process(message: string | ArrayBuffer) {
        if (message) {
            if (message.constructor === String) {
                this.term && this.term.write(message as string);
            } else {
                const data = decode(new Uint8Array(message as ArrayBuffer, 0), msgpackOptions) as (Request | Answer);
                if (data['f']) {
                    const { i, f, c, a } = data as Request;
                    try {
                        let r = f === 'enableTerminal' && c === 'RPC' ?
                            this.enableTerminal(a[0]) :
                            await CALLBACK_SOURCES.get(c)[f](...a);

                        this.send({ i, r });
                    } catch (e) {
                        this.send({ i, e });
                    }
                } else {
                    CALLS.get(data.i).write(data);
                    CALLS.delete(data.i);
                }
            }
        }
    }

    abstract enableTerminal(_);
    abstract dispose();
    abstract send(data: Answer | Request);
}