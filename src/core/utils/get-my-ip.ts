import { isIP } from 'net';

import { promiseRequest } from './cancelable-request';

export default async function () {
    let ip = '';
    try {
        const { body } = await promiseRequest('GET', 'http://ipecho.net/plain');
        ip = body.toString().trim();
        if (!isIP(ip)) {
            throw null;
        }
    } catch (_) {
        try {
            const { body } = await promiseRequest('GET', 'http://ident.me');
            ip = body.toString().trim();
            if (!isIP(ip)) {
                throw null;
            }
        } catch (_) {
            const { body } = await promiseRequest('GET', 'http://icanhazip.com');
            ip = body.toString().trim();
            if (!isIP(ip)) {
                throw 'Invalid ip: ' + ip;
            }
        }
    }
    return ip;
}