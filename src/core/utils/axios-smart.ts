import Axios, { AxiosInstance, AxiosRequestConfig } from 'axios';

import ab2obj from '@utils/arraybuffer-to-obj';
import rndId from '@utils/rnd-id';

export const TAB_ID = rndId();

export const baseConfig: AxiosRequestConfig = Object.freeze({
    headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'X-Tab': TAB_ID
    },
    withCredentials: true,
    responseType: 'json',
    validateStatus: (status: number) => {
        return status >= 200 && status < 500;
    }
});

export class Http<T = any> {
    protected http: AxiosInstance;
    constructor(config?: AxiosRequestConfig) {
        this.http = Axios.create({
            ...baseConfig,
            ...config
        })
    }
    async get(url: string) {
        const r = await this.http.get<T>(url);
        if (r.status !== 200 && r.status !== 204 && r.status !== 304) {
            throw await ab2obj(<any>r.data);
        }
        return r;
    }
    async post(url: string, payload?) {
        const r = await this.http.post<T>(url, payload);
        if (r.status !== 201) {
            throw await ab2obj(<any>r.data);
        }
        return r;
    }
    async put(url: string, payload?) {
        const r = await this.http.put<T>(url, payload);
        if (r.status !== 201) {
            throw await ab2obj(<any>r.data);
        }
        return r;
    }
    async patch(url: string, payload?) {
        const r = await this.http.patch<T>(url, payload);
        if (r.status !== 201) {
            throw await ab2obj(<any>r.data);
        }
        return r;
    }
    async delete(url: string) {
        const r = await this.http.delete(url);
        if (r.status !== 204) {
            throw await ab2obj(<any>r.data);
        }
        return r;
    }
}

export const http = new Http<string>({
    responseType: 'text'
});
export const httpJson = new Http();
export const httpMsgpack = new Http<ArrayBuffer>({
    responseType: 'arraybuffer',
    headers: { ...baseConfig.headers, 'Content-Type': 'application/x-msgpack' }
});