import { IDisposable } from 'xterm';

type Connections = Set<CancelationTokenConnection>

export class CancelationTokenConnection implements IDisposable {
    constructor(private connections: Connections, private readonly cb: Function) {
        connections.add(this);
    }
    run() {
        try {
            this.cb();
        } finally {
            this.dispose();
        }
    }
    dispose() {
        if (this.connections) {
            this.connections.delete(this);
            this.connections = null;
        }
    }
}

export class CancelationTokenError extends Error { }

export class CancelationToken {
    private readonly connections: Connections = new Set();
    constructor(
        private _canceled = false
    ) { }
    get canceled() {
        return !!this._canceled;
    }
    check() {
        if (this._canceled) {
            throw new CancelationTokenError();
        }
    }
    connect(cb) {
        this.check();
        return new CancelationTokenConnection(this.connections, cb);
    }
    cancel() {
        if (!this._canceled) {
            this._canceled = true;
            let err = undefined;
            this.connections.forEach(c => {
                try {
                    c.run();
                } catch (e) {
                    err = e || err;
                }
            });
            this.connections.clear();
            if (err !== undefined) {
                throw err;
            }
        }
    }
}