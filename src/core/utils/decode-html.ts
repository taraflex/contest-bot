import { decode } from 'iconv-lite';
import * as lastMatch from 'last-match';

const CHARSET_RE = /charset\s*=\s*"*\s*([-a-z0-9]+)/i;

export default (mime: string, buf: Buffer) => {
    let charset = mime ? lastMatch(mime, CHARSET_RE) : null;
    let uBuf = null;
    if (!charset) {
        uBuf = buf.toString('utf-8');
        charset = lastMatch(uBuf, CHARSET_RE);
    }
    charset = (charset || 'utf-8').toLowerCase().replace('windows-', 'win');
    return charset === 'utf-8' && uBuf ? uBuf : decode(buf, charset);
}