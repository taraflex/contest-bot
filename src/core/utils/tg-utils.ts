import * as TelegramBot from 'node-telegram-bot-api';

import { promiseRequest } from '@utils/cancelable-request';

export function getCommandParser(cmd: string) {
    const re = new RegExp('^\\/' + cmd + '(\\s|$)', 'i');
    return (text: string): string => re.test(text) ? text.substr(cmd.length + 2).trim() : null;
}

export const startCmdPayload = getCommandParser('start');

export async function getBotInfo(token: string): Promise<TelegramBot.User> {
    const { body } = await promiseRequest('GET', `https://api.telegram.org/bot${token}/getMe`);
    const bot: TelegramBot.User = JSON.parse(body).result;
    if (!bot.is_bot) {
        throw 'Invalid token: ' + token;
    }
    return bot;
}

export const BOT_TOKEN_RE = /^\d{9}:[\w-]{35}$/;
export const API_HASH_RE = /^[0-9abcdef]{32}$/;