import * as cheerio from 'cheerio';
import { Options } from 'request';

import { get, promiseRequest } from '@utils/cancelable-request';
import { CancelationToken } from '@utils/cancelation-token';
import decodeHtml from '@utils/decode-html';

export default async function (options: Options, cancelationToken?: CancelationToken) {
    const { body, headers } = await (cancelationToken ? get(options, cancelationToken) : promiseRequest('GET', options));
    return cheerio.load(decodeHtml(headers['content-type'], <any>body), { decodeEntities: false });
}