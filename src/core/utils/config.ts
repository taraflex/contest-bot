import * as c from 'convict';

export const CONFIG_LOCATION = __dirname + '/config.json';
const convict = c({
    ip: {
        doc: 'The IP address to bind.',
        format: 'ipaddress',
        default: '0.0.0.0',
        env: 'REGULUS_IP',
        arg: 'ip'
    },
    port: {
        doc: 'The port to bind.',
        format: 'port',
        default: 8080,
        env: 'REGULUS_PORT',
        arg: 'port'
    },
    autosave: {
        doc: 'Autosave database to file.',
        format: 'Boolean',
        default: true,
        env: 'REGULUS_AUTOSAVE',
        arg: 'autosave'
    },
    autosaveInterval: {
        doc: 'Autosave database to file interval in milliseconds.',
        format: 'nat',
        default: 20 * 1000,
        env: 'REGULUS_AUTOSAVE_INTERVAL',
        arg: 'autosave-interval'
    },
    memory: {
        doc: 'Maximum allowed memory size for nodejs',
        format: 'nat',
        default: 512,
        env: 'REGULUS_MEMORY',
        arg: 'memory'
    },
    ngrokRegion: {
        doc: 'ngrok region: us, eu, au, ap',
        format: String,
        default: 'eu',
        env: 'NGROK_REGION',
        arg: 'ngrok-region'
    }
});
try {
    convict.loadFile(CONFIG_LOCATION);
} catch (err) {
    if (!err || err.code != 'ENOENT') {
        LOG_ERROR(err);
    }
}
convict.validate();

export function getDef(prop: string) {
    return convict.default(prop);
}

export const config = Object.freeze(convict.getProperties());

export const NGROK_REGION: string = convict.get('ngrokRegion').toLowerCase();
export const MEMORY: number = convict.get('memory');
export const PORT: number = convict.get('port');
export const IP: string = convict.get('ip');
export const AUTOSAVE: boolean = convict.get('autosave');
export const AUTOSAVE_INTERVAL: number = convict.get('autosaveInterval');

export function specializePath(p: string) {
    return `/${PORT}${p}`;
}

export const ADMIN = specializePath('');
export const STATIC = specializePath('/static/');
export const API = specializePath('/api');
export const HEALTH = specializePath('/health');
export const HOOKS = specializePath('/hooks');