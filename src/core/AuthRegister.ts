import { compare } from 'bcryptjs';
import { Context, Middleware } from 'koa';
import * as compose from 'koa-compose';
import * as passport from 'koa-passport';
import * as session from 'koa-session';
import { Strategy } from 'passport-local';
import { Inject, Singleton } from 'typescript-ioc';

import { User, UserRepository } from '@entities/User';
import { PORT } from '@utils/config';
import { DEFAULT_COOKIE_OPTIONS, deleteCookie } from '@utils/server-cookie';
import trim from '@utils/trim';

import { Koa } from './Koa';

const SESSION_COOKIE_LITERAL = '_s' + PORT;

export function destroySession(ctx: Context) {
    ctx.session = null;
    deleteCookie(SESSION_COOKIE_LITERAL, ctx, true);
}

@Singleton
export class AuthRegister {
    readonly session: Middleware;
    readonly passport: Middleware;
    readonly passportSession: Middleware;
    readonly passportComposed: Middleware;

    constructor(
        @Inject userRepository: UserRepository,
        @Inject app: Koa
    ) {

        passport.serializeUser((user: User, done: Function) => {
            done(null, user.id);
        });

        passport.deserializeUser((id: number, done) => {
            userRepository.findOneOrFail(id)
                .then(user => done(null, user))
                .catch(err => done(err, false));
        });

        passport.use(new Strategy({
            usernameField: 'email',
            passwordField: 'password'
        }, async (email: string, password: string, done: Function) => {
            try {
                email = trim(email);
                password = trim(password);
                const user = await userRepository.findOneOrFail({ email })
                if (await compare(password, user.password)) {
                    done(null, user);
                } else {
                    done(null, false);
                }
            } catch (err) {
                done(err, false);
            }
        }));

        this.session = session(<any>{
            ...DEFAULT_COOKIE_OPTIONS,
            key: SESSION_COOKIE_LITERAL,
            maxAge: 86400000 * 365,
            renew: true,
            signed: true
        }, app);
        this.passport = passport.initialize();
        this.passportSession = passport.session();
        this.passportComposed = compose([this.session, this.passport, this.passportSession]);
    }
}