import { hash } from 'bcryptjs';
import { Context } from 'koa';
import * as bodyParser from 'koa-body';
import { RouterContext } from 'koa-router';
import { setInterval } from 'timers';
import { Inject, Singleton } from 'typescript-ioc';

import { EntityClass } from '@crud/types';
import { User, UserRepository } from '@entities/User';
import catchError from '@middlewares/redirect-after-error';
import detectEmailService from '@utils/detect-email-service';
import { rndString } from '@utils/rnd';
import { get, post } from '@utils/routes-helpers';
import sendEmail from '@utils/send-email';
import trim from '@utils/trim';

const RESET_PASSWORD_TIMEOUT = 60000 * 20;

@Singleton
export class ResetPassRoutes {
    private readonly resetPasswordTokens: { [token: string]: { id: number, time: number } } = Object.create(null);

    constructor(@Inject private readonly userRepository: UserRepository) {
        setInterval(() => {
            const NOW = Date.now();
            for (let token in this.resetPasswordTokens) {
                if (NOW - this.resetPasswordTokens[token].time > RESET_PASSWORD_TIMEOUT) {
                    delete this.resetPasswordTokens[token];
                }
            }
        }, RESET_PASSWORD_TIMEOUT).unref();
    }

    @get({ name: 'resetpass' })
    resetpass(ctx: Context) {
        ctx.session.canreset = null;
        ctx.pug('resetpass', { email: ctx.isAuthenticated() ? ctx.state.user.email : '' });
    }

    @post({ path: '/resetpass' }, catchError('resetpass'), bodyParser({
        text: false,
        json: false
    }))
    async resetpassPost(ctx: Context) {
        ctx.session.canreset = null;
        let { email } = <any>ctx.request.body;
        email = trim(email);

        const user = await this.userRepository.findOne({ email });

        if (!user) {
            throw 'User not found';
        }

        const { id } = user;
        for (let t in this.resetPasswordTokens) {
            if (this.resetPasswordTokens[t].id === id) {
                delete this.resetPasswordTokens[t];
            }
        }
        const token = await rndString();
        this.resetPasswordTokens[token] = { id, time: Date.now() };

        await sendEmail(email, 'Изменение пароля ' + ctx.resolve('root'), 'Для продолжения смены пароля перейдите на ' + ctx.resolve('resetpass') + '/' + token);

        ctx.pug('resetpass-sended', { email, service: detectEmailService(email) });
    }

    @get({ path: '/resetpass/:token' }, catchError('resetpass'))
    async resetToken(ctx: RouterContext) {
        ctx.session.canreset = null;
        let token = this.resetPasswordTokens[ctx.params.token];
        if (!token) {
            throw 'Invalid or expiried recovery url';
        }

        delete this.resetPasswordTokens[ctx.params.token];

        const { id, time } = token;
        if (Date.now() - time > RESET_PASSWORD_TIMEOUT) {
            throw 'Expiried recovery url';
        }

        const user = id && await this.userRepository.findOne(id);

        if (!user) {
            throw 'Пользователь был удален. Попробуйте другой аккаунт.';
        }

        ctx.logout();
        ctx.session.canreset = id;
        ctx.pug('newpass', {
            action: ctx.resolve('newpassword')
        });
    }

    @post({ name: 'newpassword', path: '/newpassword' }, catchError('login'), bodyParser({
        text: false,
        json: false
    }))
    async newpassword(ctx: Context) {
        const id = ctx.session.canreset;
        ctx.session.canreset = null;
        const user = id && await this.userRepository.findOne(id);

        if (!user) {
            throw 'Пользователь был удален. Попробуйте другой аккаунт.';
        }

        let { password } = <any>ctx.request.body;
        password = trim(password);

        (<any>User as EntityClass).insertValidate({
            email: user.email,
            password
        });

        user.password = await hash(password, 10);
        await this.userRepository.save(user);
        await ctx.login(user);
        ctx.namedRedirect('root');
    }
}