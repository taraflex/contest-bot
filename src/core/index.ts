import './preload';

import { createServer, IncomingMessage, OutgoingHttpHeaders } from 'http';
import { Context } from 'koa';
import * as mount from 'koa-mount';
import * as send from 'koa-send';
import { isIP } from 'net';
import { normalize } from 'path';
import { setInterval } from 'timers';
import { Container, Inject, Singleton } from 'typescript-ioc';
import { URL } from 'url';
import { Server as WSServer } from 'ws';

import { Settings, SettingsRepository } from '@entities/Settings';
import { ADMIN, API, IP, NGROK_REGION, PORT, STATIC } from '@utils/config';
import getMyIp from '@utils/get-my-ip';
import { ChainWebSoket, ServerRPC } from '@utils/ServerRPC';
import { genSslCert, isValidSslCert } from '@utils/ssl-utils';

//@ts-ignore
import { App } from '../App';
import { AdminRouter } from './AdminRouter';
import { ApiRouter } from './ApiRouter';
import { AuthRegister } from './AuthRegister';
import { dbInit } from './DBConnection';
import { Koa } from './Koa';
import { MainRouter } from './MainRouter';

@Singleton
class Main {
    constructor(
        @Inject private readonly settings: Settings,
        @Inject private readonly settingsRepository: SettingsRepository,
        @Inject private readonly baseApp: App,
        @Inject private readonly authRegister: AuthRegister,
        @Inject private readonly app: Koa,
        @Inject private readonly router: MainRouter,
        @Inject private readonly apiRouter: ApiRouter,
        @Inject private readonly adminRouter: AdminRouter
    ) { }

    async main() {
        const ngrokUrl = DEBUG && await require('ngrok').connect({
            proto: 'http',
            addr: 8080,
            region: NGROK_REGION,
            binPath: _ => process.cwd() + '/node_modules/ngrok/bin/'
        });

        const { router, apiRouter, adminRouter, settings, settingsRepository, app, baseApp } = this;

        if (settings.rootUrl) {
            if (ngrokUrl) {
                const u = new URL(settings.rootUrl);
                const { host } = new URL(ngrokUrl);
                if (u.host != host) {
                    u.host = host;
                    settings.rootUrl = u.toString();
                    await settingsRepository.save(settings);
                }
            } else {
                const u = new URL(settings.rootUrl);
                if (isIP(u.hostname)) {
                    const ip = await getMyIp();
                    if (ip !== u.hostname) {
                        u.hostname = ip;
                        settings.rootUrl = u.toString();
                        await settingsRepository.save(settings);
                        console.log('Server root url updated to ' + settings.rootUrl);
                    }
                    if (!await isValidSslCert(ip)) {
                        await genSslCert(ip);
                    }
                }
            }
        }

        await baseApp.init();

        router.use(API, apiRouter.routes(), apiRouter.allowedMethods())
        router.use(ADMIN, adminRouter.routes(), adminRouter.allowedMethods())

        app
            //routes
            .use(router.routes())
            .use(router.allowedMethods())
            //static assets
            .use(mount(STATIC, (ctx: Context) => send(ctx, ctx.path, {
                root: normalize(__dirname + '/static/'),
                maxage: 365 * 24 * 60 * 60 * 1000,
                gzip: !DEBUG,
                brotli: false
            })));

        const server = createServer(app.callback());

        const { passportComposed } = this.authRegister;

        const wss = new WSServer({
            server,
            verifyClient: (
                info: { origin: string; secure: boolean; req: IncomingMessage }
                , callback: (res: boolean, code?: number, message?: string, headers?: OutgoingHttpHeaders) => void
            ) => {
                try {
                    const ctx = app.createContext(info.req, null);
                    passportComposed(ctx, () => {
                        callback(!!ctx.state.user);
                        return null;
                    }).catch(console.error);
                } catch (err) {
                    LOG_ERROR(err);
                }
            }
        });

        wss.on('connection', async (socket, request) => {
            try {
                const ctx = app.createContext(request, null);
                await passportComposed(ctx, () => {
                    let terminalSize = ctx.query.cols && ctx.query.rows ? {
                        cols: +ctx.query.cols || 100,
                        rows: +ctx.query.rows || 40
                    } : null;
                    new ServerRPC(socket, parseInt(ctx.query.tab), ctx.state.user, terminalSize);
                    return null;
                });
            } catch (err) {
                LOG_ERROR(err);
                socket.close();
            }
        });

        await new Promise(resolve => server.listen(PORT, IP, resolve));

        setInterval(() => {
            wss.clients.forEach((socket: ChainWebSoket) => {
                try {
                    const { _chain } = socket;
                    if (_chain) {
                        _chain.checkActive();
                    } else {
                        socket.close();
                    }
                } catch (err) {
                    LOG_ERROR(err);
                }
            });
        }, 30000).unref();

        console.log(`Server listening on ${(ngrokUrl || settings.rootUrl || `http://${IP}${ADMIN}`)}/${PORT}`);
    }
}

dbInit()
    .then(() => (Container.get(Main) as Main).main())
    .catch(err => {
        LOG_ERROR(err);
        process.exit(1);
    });