import { Context } from 'koa';
import { Column, PrimaryGeneratedColumn, Repository } from 'typeorm';

import { SingletonEntity } from '@ioc';
import { seedRndString } from '@utils/rnd';

@SingletonEntity(null, PROJECT_SETTINGS_EXIST)
export class Settings {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ default: seedRndString() })
    secret: string;

    @Column({ default: false })
    installed: boolean;

    @Column({ default: '' })
    rootUrl: string;

    updateRootUrl(ctx: Context) {
        const url = ctx.resolve('root');
        if (this.rootUrl !== url) {
            this.rootUrl = url;
            return true;
        }
        return false;
    }
}

export abstract class SettingsRepository extends Repository<Settings>{ }