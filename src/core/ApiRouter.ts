import * as etag from 'etag';
import { Context } from 'koa';
import * as Router from 'koa-router';
import { encode } from 'msgpack-lite';
import * as pluralize from 'pluralize';
import { ObjectLiteral, Repository } from 'typeorm';
import { Container, Inject, Singleton } from 'typescript-ioc';

import { has, RTTI, RTTIItemState } from '@crud/types';
import { entities } from '@entities';
import bodyParseMsgpack from '@middlewares/body-parse-msgpack';
import checkEntityAccess from '@middlewares/check-entity-access';
import { setNoStoreHeader } from '@middlewares/no-store';
import msgpackOptions from '@utils/msgpack-options';

import { AuthRegister } from './AuthRegister';
import { DBConnection } from './DBConnection';

const paramCase = require('param-case');

export function data2Model(ctx: Context, model, repository: Repository<ObjectLiteral>) {
    const { body } = ctx.request;
    const { props } = model.constructor.rtti as RTTI;
    for (let k in body) {
        const p = props[k];
        if (p && !has(p, RTTIItemState.READONLY)) {
            model[k] = body[k];
        }
    }
    return repository.save(model);
}

export function msgpack(ctx: Context, data: any) {
    ctx.type = 'application/x-msgpack';
    const body = Array.isArray(data) ?
        (data.length > 0 ? encode({
            fields: Object.keys(data[0]),
            values: data.map(v => Object.values(v))
        }, msgpackOptions) : null) :
        (data != null ? encode(data, msgpackOptions) : null);

    if (ctx.method === 'GET') {//todo short data send as is
        const et = body && etag(body);
        if (et) {
            ctx.set('Cache-Control', 'no-cache, max-age=31536000');
            ctx.set('ETag', et);
            if (ctx.headers['if-none-match'] && ctx.headers['if-none-match'].endsWith(et)) {
                ctx.status = 304;
                return;
            }
        } else {
            setNoStoreHeader(ctx);
        }
    }
    ctx.body = body;
}

@Singleton
export class ApiRouter extends Router {

    constructor(@Inject connection: DBConnection, @Inject { session, passport, passportSession }: AuthRegister) {
        super();
        this.use(session, passport, passportSession);
        for (let e of entities) {
            const repository = connection.getRepository(e);

            const access = checkEntityAccess(e);
            const name = paramCase(pluralize(e.name));
            const singltonInstance = e.asyncProvider ? Container.get(e) : null;

            const saveModel = singltonInstance ? async (ctx: Context, model: any) => {
                const o = await data2Model(ctx, model, repository);
                const { props } = e.rtti;
                for (let p in props) {
                    singltonInstance[p] = o[p];
                    if (!has(props[p], RTTIItemState.SEND_ALWAYS)) {
                        delete o[p];
                    }
                }
                ctx.status = 201;
                msgpack(ctx, o);
            } : async (ctx: Context, model: any) => {
                const o = await data2Model(ctx, model, repository);
                const { props } = e.rtti;
                for (let p in props) {
                    if (!has(props[p], RTTIItemState.SEND_ALWAYS)) {
                        delete o[p];
                    }
                }
                ctx.status = 201;
                msgpack(ctx, o);
            }

            this
                .get(name, '/' + name, access, async (ctx: Context) => {
                    msgpack(ctx, await repository.find());
                })
                .get(`/${name}/:id`, access, async (ctx: Router.RouterContext) => {
                    const model = await repository.findOne(ctx.params.id);
                    if (!model) {
                        throw 404;
                    }
                    msgpack(ctx, model);
                })
                .put('/' + name, access, bodyParseMsgpack, (ctx: Router.RouterContext) => saveModel(ctx, new e()))
                .patch(`/${name}/:id`, access, bodyParseMsgpack, async (ctx: Router.RouterContext) => {
                    const model = await repository.findOne(ctx.params.id);
                    if (!model) {
                        throw 404;
                    }
                    await saveModel(ctx, model);
                })
                .delete(`/${name}`, access, async (ctx: Context) => {
                    await repository.clear();
                    ctx.body = null;
                })
                .delete(`/${name}/:id`, access, async (ctx: Router.RouterContext) => {
                    await repository.delete(ctx.params.id);
                    ctx.body = null;
                });
        }
    }
}