import { ElMessageBox } from 'element-ui/types/message-box';
import { Context } from 'koa';
import * as sanitize from 'sanitize-filename';
import { Client } from 'tdl';
import { Update } from 'tdl/types/tdlib';
import { IDisposable } from 'xterm';

import { PORT } from '@utils/config';
import dataPath from '@utils/data-path';
import { sendErrorEmail } from '@utils/send-email';
import { remote } from '@utils/ServerRPC';
import { API_HASH_RE, BOT_TOKEN_RE, getBotInfo } from '@utils/tg-utils';

export interface AuthInfo {
    readonly apiHash: string;
    readonly apiId: number;
}

export interface UserAuthInfo extends AuthInfo {
    readonly type: 'user';
    readonly phone: string;
}

export interface BotAuthInfo extends AuthInfo {
    readonly type: 'bot';
    readonly token: string;
}

export abstract class BaseTelegramClient implements IDisposable {
    protected client: Client;

    protected apiHash: string;
    protected apiId: number;
    protected phoneOrToken: string;

    sendErrorEmail() {
        return sendErrorEmail(`Сбой в авторизации телеграм клиента. Пересохраните настройки для получения нового авторизационного кода.`);
    }

    async update(authInfo: UserAuthInfo | BotAuthInfo, ctx?: Context) {
        const { type, apiHash, apiId } = authInfo;
        if (!apiId) {
            throw 'Invalid apiId: ' + apiId;
        }
        if (type != 'user' && type != 'bot') {
            throw 'Invalid tdlib client type: ' + type;
        }
        if (!API_HASH_RE.test(apiHash)) {
            throw 'Invalid apiHash: ' + apiHash;
        }
        if (authInfo.type === 'user' && !authInfo.phone) {
            throw 'Invalid phone: ' + authInfo.phone;
        }
        if (authInfo.type === 'bot' && !BOT_TOKEN_RE.test(authInfo.token)) {
            throw 'Invalid bot token: ' + authInfo.token;
        }
        if (
            !this.client ||
            authInfo.apiHash != this.apiHash ||
            authInfo.apiId != this.apiId ||
            (authInfo.type === 'user' && authInfo.phone != this.phoneOrToken) ||
            (authInfo.type === 'bot' && authInfo.token != this.phoneOrToken)
        ) {
            const phoneOrToken = authInfo.type === 'bot' ? authInfo.token : authInfo.phone;
            const profile = sanitize(type === 'bot' ? (await getBotInfo(phoneOrToken)).id.toString() : phoneOrToken);
            try {
                this.dispose();
                const client = this.client = new Client({
                    apiId,
                    apiHash,
                    binaryPath: './libtdjson',
                    databaseDirectory: dataPath(profile + '/database'),
                    filesDirectory: dataPath(profile + '/files'),
                    verbosityLevel: 2,
                    skipOldUpdates: false,
                    useTestDc: false,
                    useMutableRename: true,
                    tdlibParameters: {
                        use_secret_chats: true,
                        device_model: APP_NAME + ' ' + PORT,
                        enable_storage_optimizer: true,
                        use_file_database: false,
                        use_chat_info_database: true,
                        use_message_database: false,
                        ignore_file_names: true
                    }
                });
                if (this['onUpdate']) {
                    client.on('update', async (_: Update) => {
                        try {
                            //@ts-ignore
                            await this.onUpdate(_);
                        } catch (err) {
                            LOG_ERROR(err);
                        }
                    });
                }
                client.on('error', err => LOG_ERROR(err));
                await client.connect();
                await client.invoke({
                    _: 'setOption',
                    name: 'prefer_ipv6',
                    value: { _: 'optionValueBoolean', value: false }
                });
                await client.invoke({
                    _: 'setOption',
                    name: 'online',
                    value: { _: 'optionValueBoolean', value: false }
                });
                this.apiHash = apiHash;
                this.apiId = apiId;
                await client.login(() => type === 'bot' ?
                    {
                        type: 'bot',
                        token: phoneOrToken
                    } : {
                        type: 'user',
                        phoneNumber: phoneOrToken,
                        getAuthCode: async (retry?: boolean) => {
                            if (!ctx) {
                                this.dispose();
                                if (!retry) {
                                    await this.sendErrorEmail();
                                }
                                throw 'Telegram client auth required.';
                            }
                            const code = await remote<ElMessageBox>('ElMessageBox', ctx).prompt('Введите код авторизации телеграм клиента (придет в sms или в уже авторизованный клиент).', {
                                showCancelButton: false,
                                inputType: 'number',
                                roundButton: true
                            });
                            return code.value || '';
                        }
                    }
                );
                this.phoneOrToken = phoneOrToken;
                return true;
            } catch (err) {
                this.dispose();
                throw err;
            }
        }
        return false;
    }

    dispose() {
        if (this.client) {
            this.client.destroy();
            this.client = null;
            this.phoneOrToken = null;
            this.apiHash = null;
            this.apiId = null;
        }
    }

    async logout() {
        if (this.client) {
            await this.client.invoke({ _: 'logOut' });
        }
        this.dispose();
    }
}