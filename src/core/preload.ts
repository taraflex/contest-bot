process.env.NTBA_FIX_319 = '1';

if (!(DEBUG || typeof v8debug === 'object' || /--debug|--inspect/.test(process.execArgv.join(' ')))) {
    const PrettyError = require('pretty-error');
    const pe = new PrettyError();
    pe.withoutColors();//в логи попадают управляющие символы отвечающие за цвет - становится нечитабельно
    pe.start();
}