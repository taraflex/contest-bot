import './index.scss';

import Vue from 'vue';

// @ts-ignore
import ErrorPlaceholder from '@frontend/ErrorPlaceholder.vue';
// @ts-ignore
import LoadingPlaceholder from '@frontend/LoadingPlaceholder.vue';

Vue.component('terminal-async', () => ({
    // @ts-ignore
    component: import( /* webpackChunkName: "terminal"*/  `./TerminalAsync.vue`),
    loading: LoadingPlaceholder,
    error: ErrorPlaceholder,
    delay: 0
}));

export default {
    name: 'Terminal',
    icon: 'terminal'
}