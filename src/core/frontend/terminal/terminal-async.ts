import { Terminal as XTerminal } from 'xterm';
import * as webLinks from 'xterm/lib/addons/webLinks/webLinks';

import { notify } from '@frontend/error-handler-mixin';

XTerminal.applyAddon(webLinks);

export default {
    data() {
        return { loading: true }
    },
    async mounted() {
        try {
            if (!this.t) {
                const t = this.t = new XTerminal({
                    cols: 100,
                    rows: 40,
                    cursorBlink: true,
                    cursorStyle: 'bar',
                    fontSize: 13,
                    fontFamily: 'Consolas, monaco, monospace'
                });
                t.open(this.$refs.terminal);
                //@ts-ignore
                t.webLinksInit();
                await this.$rpc.enableTerminal(t);
            }
        } catch (err) {
            notify(err);
        } finally {
            this.loading = false;
        }
    },
    beforeDestroy() {
        this.t && this.t.dispose();
    }
}