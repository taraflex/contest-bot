import Vue from 'vue';

// @ts-ignore
import CodeMirror from './CodeMirror.vue';

Vue.component(CodeMirror.name, CodeMirror);