import './telegram-markdown';

import { Editor, fromTextArea } from 'codemirror';

export default {
    name: 'code-mirror',
    props: {
        value: String,
        readonly: Boolean
    },
    beforeCreate() {
        this._onChangeHandler = function (cm: Editor) {
            const v = cm.getValue();
            this.$emit('input', v);
        }.bind(this);
    },
    mounted() {
        this.editor = fromTextArea(this.$el as HTMLTextAreaElement, {
            lineWrapping: true,
            theme: 'brackets',
            mode: 'telegram-markdown',
            smartIndent: false,
            dragDrop: false,
            indentUnit: 4,
            tabSize: 4,
            undoDepth: 100,
            autofocus: false,
            pollInterval: 500,
            workDelay: 500,
            readOnly: this.readonly
        });
        this.editor.setValue(this.value);
        this.editor.on('change', this._onChangeHandler);
    },
    watch: {
        readonly(v) {
            this.editor && this.editor.setOption('readOnly', v);
        },
        value(v) {
            v = v || '';
            if (this.editor && v !== this.editor.getValue()) {
                const scrollInfo = this.editor.getScrollInfo();
                this.editor.off('change', this._onChangeHandler);
                this.editor.setValue(v);
                this.editor.scrollTo(scrollInfo.left, scrollInfo.top);
                this.editor.on('change', this._onChangeHandler);
            }
        }
    },
    beforeDestroy() {
        if (this.editor) {
            this.editor.off('change', this._onChangeHandler);
            this.editor.toTextArea();
            this.editor = undefined;
        }
    }
}