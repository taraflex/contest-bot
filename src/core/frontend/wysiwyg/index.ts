import Vue from 'vue';
import wysiwyg from 'vue-wysiwyg';

Vue.use(wysiwyg, {
    hideModules: {
        separator: true,
        image: true
    }
});