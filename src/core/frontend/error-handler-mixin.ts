import { Notification } from 'element-ui';
import Vue from 'vue';
import Component from 'vue-class-component';

import { FlashLevel } from '@middlewares/flash';
import { delay } from '@taraflex/debounce';
import { httpJson } from '@utils/axios-smart';
import { ClientRPC } from '@utils/ClientRPC';
import stringify from '@utils/stringify';

export function notify(err, level?: FlashLevel) {
    switch (level || FlashLevel.ERROR) {
        case FlashLevel.ERROR:
            LOG_ERROR(err);
            Notification.error(<any>{
                message: stringify(err),
                position: 'bottom-right',
                duration: 0
            });
            break;
        case FlashLevel.WARNING:
            console.warn(err);
            Notification.warning(<any>{
                message: stringify(err),
                position: 'bottom-right',
                duration: 0
            });
            break;
        default:
            console.log(err);
            Notification.info(<any>{
                message: stringify(err),
                position: 'bottom-right'
            });
            break;
    }
}

@Component
export class ErrorHandlerMixin extends Vue {
    loading = false;
    onError(errors) {
        this.loading = false;
        errors = errors.errors || errors;
        if (errors && typeof (errors.forEach) === 'function') {
            errors.forEach(e => notify(e));
        } else {
            notify(errors);
        }
    }
    run(fn, ...args) {
        fn && this[fn](...args);
    }
    logout() {
        window.location.href = Paths.logout;
    }
    changePassword() {
        window.location.href = Paths.resetpass;
    }
    async waitServerRestart() {
        for (; ;) {
            try {
                (this.$rpc as ClientRPC).dispose();
                await delay(7000);
                if ((await httpJson.get(Paths.health)).data.success === 'ok') {
                    window.location.reload(true);
                }
            } catch (_) { }
        }
    }
    async restartServer() {
        try {
            this.loading = true;
            await httpJson.put(Paths.restart);
            await this.waitServerRestart();
        } catch (err) {
            this.onError(err);
        } finally {
            this.loading = false;
        }
    }
}