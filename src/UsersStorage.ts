import { readFileSync } from 'fs';
import { writeFile } from 'fs-extra';

import { debounce } from '@taraflex/debounce';
import { AUTOSAVE_INTERVAL } from '@utils/config';
import dataPath from '@utils/data-path';

const path = dataPath('users.bin');

export class UsersStorage {
    users: Set<number>;
    save = debounce(function () {
        return writeFile(path, new Uint32Array(this.users));
    }, AUTOSAVE_INTERVAL);
    constructor() {
        try {
            const b = readFileSync(path);
            this.users = new Set(new Uint32Array(b.buffer, b.byteOffset, b.byteLength / Uint32Array.BYTES_PER_ELEMENT));
        } catch (err) {
            this.users = new Set();
            if (!err || err.code != 'ENOENT') {
                LOG_ERROR(err);
            }
        }
    }
    exist(id: number) {
        return this.users.has(id);
    }
    add(id: number) {
        const s = this.users.size;
        this.users.add(id);
        if (this.users.size != s) {
            this.save();
        }
    }
    remove(id: number) {
        const s = this.users.size;
        this.users.delete(id);
        if (this.users.size != s) {
            this.save();
        }
    }
}