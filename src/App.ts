import * as hogan from 'hogan.js';
import { Context } from 'koa';
import { Inject, Singleton } from 'typescript-ioc';

import { EntityClass, has, RTTIItemState } from '@crud/types';
import { ChannelRepository } from '@entities/Channel';
import { ClientSettings, ClientSettingsRepository } from '@entities/ClientSettings';
import bodyParseMsgpack from '@middlewares/body-parse-msgpack';
import checkEntityAccess from '@middlewares/check-entity-access';
import smartRedirect from '@middlewares/smart-redirect';
import { specializePath } from '@utils/config';
import { toRouter } from '@utils/routes-helpers';
import { RPC } from '@utils/RPC';
import { sendErrorEmail } from '@utils/send-email';
import stringify from '@utils/stringify';

import { ContestBot } from './ContestBot';
import { ApiRouter, msgpack } from './core/ApiRouter';
import { MainRouter } from './core/MainRouter';
import { MessageTemplates } from './MessageTemplates';

@Singleton
export class App {
    constructor(
        @Inject private readonly bot: ContestBot,
        @Inject private readonly settings: ClientSettings,
        @Inject private readonly channelRepository: ChannelRepository,
        @Inject private readonly templates: MessageTemplates,
        @Inject settingsRepository: ClientSettingsRepository,
        @Inject apiRouter: ApiRouter,
        @Inject mainRouter: MainRouter
    ) {
        RPC.register('notify', {
            brodcast() {
                return bot.brodcast();
            },
            cancelBrodcast() {
                return bot.cancelBrodcast();
            },
            getQueueSize() {
                return bot.queueSize;
            }
        });

        apiRouter.patch('tgclient_save', '/tgclient-save/:id', checkEntityAccess(ClientSettings), bodyParseMsgpack, smartRedirect, async (ctx: Context) => {
            const body: ClientSettings = <any>ctx.request.body;

            (ClientSettings as EntityClass<ClientSettings>).insertValidate(body);

            const templates = await this.buildMessages(body);

            try {
                await bot.update({ type: 'bot', ...body }, ctx);
            } catch (err) {
                LOG_2ERRORS(body, err);
                throw [{
                    field: '_',
                    message: stringify(err)
                }];
            }

            const { props } = (ClientSettings as EntityClass).rtti;
            for (let k in body) {
                const p = props[k];
                if (p && !has(p, RTTIItemState.HIDDEN)) {
                    this.settings[k] = body[k];
                }
            }

            await settingsRepository.save(this.settings);

            Object.assign(this.templates, templates);

            ctx.status = 201;
            msgpack(ctx, this.settings);
        });

        const botRouter = toRouter(bot);
        mainRouter.use(specializePath('/xlsx'), botRouter.routes(), botRouter.allowedMethods());
    }

    async buildMessages(s: ClientSettings) {
        const templates = new MessageTemplates();
        const chats = await this.channelRepository.find({ required: true });
        for (let p of ['infoMessage', 'startMessage', 'okMessage', 'errMessage', 'cancelMessage']) {
            try {
                templates[p] = hogan.compile(s[p]).render({ chats });
            } catch (err) {
                LOG_2ERRORS(s[p], err);
                throw [{
                    field: p,
                    message: 'Invalid template'
                }];
            }
        }
        return templates;
    }

    async updateTemplates() {
        Object.assign(this.templates, await this.buildMessages(this.settings));
    }

    async init() {
        if (this.settings.token) {
            try {
                await this.bot.update({ type: 'bot', ...this.settings });
            } catch (err) {
                await sendErrorEmail('Ошибка запуска бота. Проверьте токен телеграм бота.');
                throw err;
            }
        }
        await this.updateTemplates();
    }
}