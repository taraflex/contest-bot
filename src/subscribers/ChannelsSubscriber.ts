import { EntitySubscriberInterface, EventSubscriber } from 'typeorm';
import { Container } from 'typescript-ioc';

import { Channel } from '@entities/Channel';

import { App } from '../App';

@EventSubscriber()
export class ChannelsSubscriber implements EntitySubscriberInterface<Channel> {

    listenTo() {
        return Channel;
    }

    afterUpdate() {
        return (Container.get(App) as App).updateTemplates();
    }

    afterRemove() {
        return (Container.get(App) as App).updateTemplates();
    }
}