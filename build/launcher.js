!function(modules) {
    var installedModules = {};
    function __webpack_require__(moduleId) {
        if (installedModules[moduleId]) return installedModules[moduleId].exports;
        var module = installedModules[moduleId] = {
            i: moduleId,
            l: !1,
            exports: {}
        };
        return modules[moduleId].call(module.exports, module, module.exports, __webpack_require__), 
        module.l = !0, module.exports;
    }
    __webpack_require__.m = modules, __webpack_require__.c = installedModules, __webpack_require__.d = function(exports, name, getter) {
        __webpack_require__.o(exports, name) || Object.defineProperty(exports, name, {
            enumerable: !0,
            get: getter
        });
    }, __webpack_require__.r = function(exports) {
        "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(exports, Symbol.toStringTag, {
            value: "Module"
        }), Object.defineProperty(exports, "__esModule", {
            value: !0
        });
    }, __webpack_require__.t = function(value, mode) {
        if (1 & mode && (value = __webpack_require__(value)), 8 & mode) return value;
        if (4 & mode && "object" == typeof value && value && value.__esModule) return value;
        var ns = Object.create(null);
        if (__webpack_require__.r(ns), Object.defineProperty(ns, "default", {
            enumerable: !0,
            value
        }), 2 & mode && "string" != typeof value) for (var key in value) __webpack_require__.d(ns, key, function(key) {
            return value[key];
        }.bind(null, key));
        return ns;
    }, __webpack_require__.n = function(module) {
        var getter = module && module.__esModule ? function getDefault() {
            return module.default;
        } : function getModuleExports() {
            return module;
        };
        return __webpack_require__.d(getter, "a", getter), getter;
    }, __webpack_require__.o = function(object, property) {
        return Object.prototype.hasOwnProperty.call(object, property);
    }, __webpack_require__.p = "", __webpack_require__(__webpack_require__.s = 394);
}({
    11: function(module, __webpack_exports__, __webpack_require__) {
        "use strict";
        __webpack_require__.d(__webpack_exports__, "a", function() {
            return stringify;
        });
        var tosource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(28);
        const stdNames = [ "Error", "Object" ];
        function stringify(o) {
            if (!o || o === !!o || o === +o) return String(o);
            if (o.constructor === String) return o;
            if (o.hasOwnProperty("toString") || "symbol" == typeof o) return o.toString();
            if (o.hasOwnProperty("toJSON")) return JSON.stringify(o, null, "  ");
            delete o.stack;
            const info = o.message ? stringify(o.message) : tosource__WEBPACK_IMPORTED_MODULE_0__(o), name = o.name || o.constructor.name;
            return name && -1 === stdNames.indexOf(name) ? name + ": " + info : info;
        }
    },
    115: function(module, exports) {
        module.exports = require("json5");
    },
    15: function(module, exports) {
        module.exports = require("fs-extra");
    },
    16: function(module, __webpack_exports__, __webpack_require__) {
        "use strict";
        var _utils_stringify__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(11);
        __webpack_exports__.a = function(s) {
            return s ? Object(_utils_stringify__WEBPACK_IMPORTED_MODULE_0__.a)(s).trim() : "";
        };
    },
    188: function(module, exports, __webpack_require__) {
        "use strict";
        var isArray = Array.isArray, keyList = Object.keys, hasProp = Object.prototype.hasOwnProperty;
        module.exports = function equal(a, b) {
            if (a === b) return !0;
            if (a && b && "object" == typeof a && "object" == typeof b) {
                var i, length, key, arrA = isArray(a), arrB = isArray(b);
                if (arrA && arrB) {
                    if (length = a.length, length != b.length) return !1;
                    for (i = length; 0 != i--; ) if (!equal(a[i], b[i])) return !1;
                    return !0;
                }
                if (arrA != arrB) return !1;
                var dateA = a instanceof Date, dateB = b instanceof Date;
                if (dateA != dateB) return !1;
                if (dateA && dateB) return a.getTime() == b.getTime();
                var regexpA = a instanceof RegExp, regexpB = b instanceof RegExp;
                if (regexpA != regexpB) return !1;
                if (regexpA && regexpB) return a.toString() == b.toString();
                var keys = keyList(a);
                if (length = keys.length, length !== keyList(b).length) return !1;
                for (i = length; 0 != i--; ) if (!hasProp.call(b, keys[i])) return !1;
                for (i = length; 0 != i--; ) if (key = keys[i], !equal(a[key], b[key])) return !1;
                return !0;
            }
            return a != a && b != b;
        };
    },
    189: function(module, exports) {
        module.exports = require("random-number");
    },
    20: function(module, __webpack_exports__, __webpack_require__) {
        "use strict";
        __webpack_require__.d(__webpack_exports__, "a", function() {
            return SSL_CERT_LOCATION;
        }), __webpack_require__.d(__webpack_exports__, "b", function() {
            return SSL_KEY_LOCATION;
        }), __webpack_require__.d(__webpack_exports__, "d", function() {
            return isValidSslCert;
        }), __webpack_require__.d(__webpack_exports__, "c", function() {
            return genSslCert;
        });
        var child_process_promise__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(21), fs_extra__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(15), ssl_validator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(80);
        const SSL_CERT_LOCATION = "/home/localhost.cert", SSL_KEY_LOCATION = "/home/localhost.key";
        async function isValidSslCert(ip) {
            try {
                await Object(ssl_validator__WEBPACK_IMPORTED_MODULE_2__.validateSSL)(await Object(fs_extra__WEBPACK_IMPORTED_MODULE_1__.readFile)(SSL_CERT_LOCATION), {
                    key: await Object(fs_extra__WEBPACK_IMPORTED_MODULE_1__.readFile)(SSL_KEY_LOCATION),
                    domain: ip
                });
            } catch (_) {
                return !1;
            }
            return !0;
        }
        function genSslCert(ip) {
            return Object(child_process_promise__WEBPACK_IMPORTED_MODULE_0__.exec)(`openssl req -newkey rsa:1024 -sha256 -nodes -keyout ${SSL_KEY_LOCATION} -x509 -days 3650 -out ${SSL_CERT_LOCATION} -subj "/C=US/ST=New York/L=Brooklyn/O=Example Company/CN=${ip}"`);
        }
    },
    21: function(module, exports) {
        module.exports = require("child-process-promise");
    },
    27: function(module, exports) {
        module.exports = require("net");
    },
    28: function(module, exports) {
        module.exports = require("tosource");
    },
    3: function(module, __webpack_exports__, __webpack_require__) {
        "use strict";
        __webpack_require__.d(__webpack_exports__, "e", function() {
            return CONFIG_LOCATION;
        }), __webpack_require__.d(__webpack_exports__, "m", function() {
            return getDef;
        }), __webpack_require__.d(__webpack_exports__, "l", function() {
            return config;
        }), __webpack_require__.d(__webpack_exports__, "i", function() {
            return MEMORY;
        }), __webpack_require__.d(__webpack_exports__, "j", function() {
            return PORT;
        }), __webpack_require__.d(__webpack_exports__, "h", function() {
            return IP;
        }), __webpack_require__.d(__webpack_exports__, "c", function() {
            return AUTOSAVE;
        }), __webpack_require__.d(__webpack_exports__, "d", function() {
            return AUTOSAVE_INTERVAL;
        }), __webpack_require__.d(__webpack_exports__, "n", function() {
            return specializePath;
        }), __webpack_require__.d(__webpack_exports__, "a", function() {
            return ADMIN;
        }), __webpack_require__.d(__webpack_exports__, "k", function() {
            return STATIC;
        }), __webpack_require__.d(__webpack_exports__, "b", function() {
            return API;
        }), __webpack_require__.d(__webpack_exports__, "f", function() {
            return HEALTH;
        }), __webpack_require__.d(__webpack_exports__, "g", function() {
            return HOOKS;
        });
        var convict__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(77);
        const CONFIG_LOCATION = __dirname + "/config.json", convict = convict__WEBPACK_IMPORTED_MODULE_0__({
            ip: {
                doc: "The IP address to bind.",
                format: "ipaddress",
                default: "0.0.0.0",
                env: "REGULUS_IP",
                arg: "ip"
            },
            port: {
                doc: "The port to bind.",
                format: "port",
                default: 8080,
                env: "REGULUS_PORT",
                arg: "port"
            },
            autosave: {
                doc: "Autosave database to file.",
                format: "Boolean",
                default: !0,
                env: "REGULUS_AUTOSAVE",
                arg: "autosave"
            },
            autosaveInterval: {
                doc: "Autosave database to file interval in milliseconds.",
                format: "nat",
                default: 2e4,
                env: "REGULUS_AUTOSAVE_INTERVAL",
                arg: "autosave-interval"
            },
            memory: {
                doc: "Maximum allowed memory size for nodejs",
                format: "nat",
                default: 512,
                env: "REGULUS_MEMORY",
                arg: "memory"
            },
            ngrokRegion: {
                doc: "ngrok region: us, eu, au, ap",
                format: String,
                default: "eu",
                env: "NGROK_REGION",
                arg: "ngrok-region"
            }
        });
        try {
            convict.loadFile(CONFIG_LOCATION);
        } catch (err) {
            err && "ENOENT" == err.code || console.error("(core/utils/config.ts:52)", err);
        }
        function getDef(prop) {
            return convict.default(prop);
        }
        convict.validate();
        const config = Object.freeze(convict.getProperties()), MEMORY = (convict.get("ngrokRegion").toLowerCase(), 
        convict.get("memory")), PORT = convict.get("port"), IP = convict.get("ip"), AUTOSAVE = convict.get("autosave"), AUTOSAVE_INTERVAL = convict.get("autosaveInterval");
        function specializePath(p) {
            return `/${PORT}${p}`;
        }
        const ADMIN = specializePath(""), STATIC = specializePath("/static/"), API = specializePath("/api"), HEALTH = specializePath("/health"), HOOKS = specializePath("/hooks");
    },
    394: function(module, __webpack_exports__, __webpack_require__) {
        "use strict";
        __webpack_require__.r(__webpack_exports__);
        var child_process_promise__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(21), fast_deep_equal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(188), fs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5), json5__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(115), make_dir__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(81), path__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(6), random_number__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(189), _utils_config__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(3), _utils_get_my_ip__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(78), _utils_ssl_utils__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(20), _utils_trim__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(16);
        function readConfig(filename) {
            try {
                return json5__WEBPACK_IMPORTED_MODULE_3__.parse(Object(fs__WEBPACK_IMPORTED_MODULE_2__.readFileSync)(filename, "utf-8"));
            } catch (_) {}
            return {};
        }
        async function e(cmd) {
            const {stdout, stderr} = await Object(child_process_promise__WEBPACK_IMPORTED_MODULE_0__.exec)(cmd), t = stdout || stderr;
            return Object(_utils_trim__WEBPACK_IMPORTED_MODULE_10__.a)(t && t.toString());
        }
        async function ec(cmd) {
            try {
                return await e(cmd);
            } catch (e) {
                const t = e.stderr || e.stdout;
                return Object(_utils_trim__WEBPACK_IMPORTED_MODULE_10__.a)(t && t.toString());
            }
        }
        (async function main() {
            let nginxPath = "/etc/nginx/sites-available/default";
            await ec("pm2 update"), await ec(`pm2 delete contest-bot-${_utils_config__WEBPACK_IMPORTED_MODULE_7__.j}`);
            const blackListPorts = new Set(json5__WEBPACK_IMPORTED_MODULE_3__.parse(await e("pm2 prettylist")).map(v => v.pm2_env.pm_exec_path).filter(v => v).map(path__WEBPACK_IMPORTED_MODULE_5__.dirname).map(v => (function readConfigPort(filename) {
                try {
                    return 0 | readConfig(filename).port;
                } catch (_) {}
                return 0;
            })(v + "/config.json")).filter(v => v).concat(Object(_utils_config__WEBPACK_IMPORTED_MODULE_7__.m)("port"), 0, 1, 1e4, 10001, 10009, 10010, 10024, 10025, 10042, 10050, 10051, 10080, 101, 1010, 1011, 10110, 10172, 102, 1020, 10200, 10201, 10204, 10212, 1023, 1024, 1027, 1028, 1029, 10308, 104, 10480, 105, 10505, 10514, 1058, 1059, 107, 108, 1080, 10823, 1085, 10891, 109, 10933, 1098, 1099, 11, 110, 11001, 1109, 111, 11111, 11112, 1119, 11211, 11214, 11215, 11235, 113, 11311, 11371, 115, 1167, 117, 11753, 118, 119, 1194, 1198, 12012, 12013, 12035, 12043, 12046, 1214, 1220, 12201, 12222, 12223, 123, 1234, 12345, 1241, 12443, 12489, 126, 1270, 1293, 12975, 13, 13e3, 13008, 13050, 13075, 1311, 1314, 1337, 1341, 1344, 135, 1352, 1360, 137, 13720, 13721, 13724, 13782, 13783, 13785, 13786, 138, 139, 1414, 1417, 1418, 1419, 1420, 143, 1431, 1433, 1434, 14550, 14567, 1492, 1494, 15, 1500, 15e3, 1501, 1503, 1512, 1513, 152, 1521, 1524, 1527, 153, 1533, 15345, 1540, 1541, 1542, 15441, 1545, 1547, 1550, 15567, 156, 1560, 15672, 158, 1581, 1582, 1583, 1589, 1591, 16e3, 1604, 16080, 161, 162, 16200, 16225, 16250, 1626, 16261, 1627, 1628, 1629, 16300, 16384, 16387, 16393, 16400, 16402, 16403, 1645, 1646, 16472, 16482, 16567, 1666, 1677, 1688, 17, 170, 1701, 17011, 1707, 1716, 1719, 1720, 1723, 17500, 1755, 1761, 177, 1783, 179, 18, 1801, 18091, 18092, 18104, 1812, 1813, 18200, 18201, 18206, 18300, 18301, 18306, 18333, 18400, 18401, 18505, 18506, 18605, 18606, 1863, 1880, 1883, 19, 1900, 19e3, 19001, 19132, 19150, 19226, 19294, 19295, 19302, 1935, 194, 1967, 1970, 1972, 19812, 19813, 19814, 1984, 1985, 199, 1998, 19999, 20, 2e3, 2e4, 201, 2010, 2033, 2049, 2056, 20560, 20595, 2080, 20808, 2082, 2083, 2086, 2087, 209, 2095, 2096, 21, 210, 2100, 2101, 2102, 21025, 2103, 2104, 2123, 213, 2142, 2152, 2159, 218, 2181, 2195, 2196, 22, 220, 22e3, 2210, 2211, 22136, 2221, 2222, 22222, 2226, 225, 2261, 2262, 2266, 23, 2302, 2303, 2305, 23073, 23399, 2351, 23513, 2368, 2369, 2370, 2372, 2375, 2376, 2377, 2379, 2380, 2399, 2401, 2404, 241, 2424, 2427, 24441, 24444, 24465, 2447, 24554, 2480, 24800, 2483, 2484, 24842, 249, 25, 2535, 2541, 2546, 2548, 255, 25565, 25575, 25826, 259, 2593, 2598, 2599, 26e3, 262, 2638, 264, 26900, 26901, 27e3, 27006, 27009, 27015, 27016, 27017, 27018, 27030, 27031, 27036, 27037, 2710, 2727, 27374, 27500, 27888, 27900, 27901, 27910, 27950, 27960, 27969, 280, 28001, 28015, 2809, 2811, 2827, 28770, 28771, 28785, 28786, 28852, 28910, 28960, 29e3, 29070, 2944, 2945, 2947, 2948, 2949, 2967, 29900, 29901, 29920, 300, 3e3, 3004, 3020, 3050, 3052, 30564, 3074, 308, 3101, 311, 3128, 31337, 31416, 31438, 31457, 318, 319, 320, 32137, 3225, 3233, 32400, 3260, 3268, 3269, 32764, 3283, 32887, 3290, 32976, 3305, 3306, 3313, 3316, 3323, 3332, 3333, 33434, 3351, 33848, 3386, 3389, 3396, 34e3, 3412, 34197, 3423, 3424, 3455, 3478, 3479, 3480, 3483, 3493, 350, 351, 3516, 3527, 3535, 35357, 3544, 356, 3632, 3645, 3659, 366, 3667, 3689, 369, 3690, 37, 370, 37008, 3702, 371, 3724, 3725, 3768, 3784, 3785, 3799, 38, 3804, 3825, 3826, 383, 3830, 3835, 384, 3856, 3868, 387, 3872, 3880, 389, 39, 3900, 3960, 3962, 3978, 3979, 399, 3999, 4e3, 4e4, 4001, 401, 4018, 4035, 4045, 4050, 4069, 4089, 4090, 4093, 4096, 4105, 4111, 4116, 4125, 4172, 4190, 4198, 42, 4201, 4222, 4226, 4242, 4243, 4244, 427, 43, 4303, 4307, 43110, 4321, 433, 434, 4352, 43594, 43595, 443, 444, 44405, 4444, 4445, 445, 44818, 4486, 4488, 4500, 4502, 4505, 4506, 4534, 4560, 4567, 4569, 4604, 4605, 4610, 464, 4640, 465, 4662, 4664, 4672, 47, 47001, 4711, 4713, 4728, 4730, 4739, 4747, 475, 4750, 4753, 47808, 4789, 4840, 4843, 4847, 4848, 4894, 49, 491, 49151, 4949, 4950, 497, 5, 50, 500, 5e3, 5001, 5002, 5003, 5004, 5005, 5010, 5011, 502, 5025, 5031, 5037, 504, 5048, 5050, 5051, 5060, 5061, 5062, 5064, 5065, 5070, 5084, 5085, 5093, 5099, 51, 510, 5104, 512, 5121, 5124, 5125, 513, 514, 515, 5150, 5151, 5154, 517, 5172, 518, 5190, 5198, 5199, 52, 520, 5200, 5201, 521, 5222, 5223, 5228, 524, 5242, 5243, 5246, 5247, 525, 5269, 5280, 5281, 5298, 53, 530, 5310, 5318, 532, 533, 5349, 5351, 5353, 5355, 5357, 5358, 5394, 54, 540, 5402, 5405, 5412, 5413, 5417, 542, 5421, 543, 5432, 5433, 544, 5445, 546, 547, 548, 5480, 5481, 5495, 5498, 5499, 550, 5500, 5501, 5517, 554, 5550, 5554, 5555, 5556, 556, 5568, 56, 560, 5601, 561, 563, 5631, 5632, 564, 5656, 5666, 5667, 5670, 5671, 5672, 5678, 5683, 57, 5701, 5718, 5719, 5722, 5723, 5724, 5741, 5742, 58, 5800, 585, 587, 5900, 591, 593, 5931, 5938, 5984, 5985, 5986, 5988, 5989, 6e3, 6005, 6009, 601, 604, 6050, 6051, 6063, 6086, 61, 6100, 6101, 6110, 6111, 6112, 6113, 6136, 6159, 6200, 6201, 6225, 6227, 623, 6240, 6244, 625, 6255, 6257, 6260, 6262, 631, 6343, 6346, 6347, 635, 6350, 636, 6379, 6389, 639, 641, 643, 6432, 6436, 6437, 6444, 6445, 646, 6463, 6464, 647, 6472, 648, 6502, 651, 6513, 6514, 6515, 653, 654, 6543, 655, 6556, 6560, 6561, 6566, 657, 6571, 660, 6600, 6601, 6602, 6619, 6622, 6653, 666, 6660, 6664, 6665, 6669, 6679, 6690, 6697, 6699, 67, 6715, 674, 6771, 6783, 6785, 6789, 68, 6869, 688, 6881, 6887, 6888, 6889, 6890, 6891, 69, 690, 6900, 6901, 6902, 691, 694, 695, 6968, 6969, 6970, 698, 6999, 7, 70, 700, 7e3, 7001, 7002, 7005, 7006, 701, 7010, 702, 7022, 7023, 7025, 7047, 706, 7070, 71, 711, 712, 7133, 7144, 7145, 7171, 7262, 7272, 7306, 7307, 7312, 7396, 74, 7400, 7401, 7402, 7471, 7473, 7474, 7478, 749, 75, 750, 751, 752, 753, 754, 7542, 7547, 7575, 760, 7624, 7631, 7634, 7652, 7654, 7655, 7656, 7660, 7670, 7687, 77, 7707, 7708, 7717, 7777, 7788, 782, 783, 7831, 7880, 7890, 79, 7915, 7935, 7946, 7990, 80, 800, 8e3, 8005, 8006, 8007, 8008, 8009, 8042, 8069, 8070, 8074, 8075, 808, 8080, 8088, 8089, 8090, 8091, 8092, 81, 8111, 8112, 8116, 8118, 8123, 8139, 8140, 8172, 8184, 8194, 8195, 82, 8200, 8222, 8243, 8245, 8280, 8281, 829, 8291, 830, 8303, 831, 832, 833, 8332, 8333, 8337, 8384, 8388, 843, 8443, 8444, 847, 848, 8484, 8500, 853, 8530, 8531, 8580, 860, 861, 862, 8629, 8642, 8691, 87, 873, 8767, 88, 8834, 8840, 888, 8880, 8883, 8887, 8888, 8889, 897, 898, 8983, 8997, 8998, 8999, 9, 90, 9e3, 9001, 9002, 9006, 902, 903, 9030, 9042, 9043, 9050, 9051, 9060, 9080, 9090, 9091, 9092, 9100, 9101, 9102, 9103, 9119, 9150, 9191, 9199, 9200, 9217, 9293, 9300, 9303, 9306, 9309, 9312, 9332, 9333, 9339, 9389, 9418, 9419, 9420, 9421, 9422, 9425, 9443, 953, 9535, 9536, 9600, 9675, 9676, 9695, 9785, 9800, 981, 987, 9875, 989, 9898, 9899, 99, 990, 991, 992, 993, 994, 995, 9981, 9982, 9987, 9993, 9997, 9999));
            let port = _utils_config__WEBPACK_IMPORTED_MODULE_7__.j;
            for (;blackListPorts.has(port) || await ec("lsof -i :" + port); ) port = random_number__WEBPACK_IMPORTED_MODULE_6__({
                min: 3e3,
                max: 49150,
                integer: !0
            });
            !function updateConfig(port) {
                const oldConfig = readConfig(_utils_config__WEBPACK_IMPORTED_MODULE_7__.e);
                for (let p in oldConfig) oldConfig[p] === Object(_utils_config__WEBPACK_IMPORTED_MODULE_7__.m)(p) && delete oldConfig[p];
                const newConfig = {
                    ..._utils_config__WEBPACK_IMPORTED_MODULE_7__.l,
                    port
                };
                for (let p in newConfig) newConfig[p] === Object(_utils_config__WEBPACK_IMPORTED_MODULE_7__.m)(p) && delete newConfig[p];
                fast_deep_equal__WEBPACK_IMPORTED_MODULE_1__(oldConfig, newConfig) || Object(fs__WEBPACK_IMPORTED_MODULE_2__.writeFileSync)(_utils_config__WEBPACK_IMPORTED_MODULE_7__.e, JSON.stringify(newConfig, null, "  "));
            }(port);
            let ip = await Object(_utils_get_my_ip__WEBPACK_IMPORTED_MODULE_8__.a)(), needGenCert = !Object(_utils_ssl_utils__WEBPACK_IMPORTED_MODULE_9__.d)(ip);
            const name = "contest-bot-" + port;
            await Promise.all([ needGenCert && Object(_utils_ssl_utils__WEBPACK_IMPORTED_MODULE_9__.c)(ip), Object(child_process_promise__WEBPACK_IMPORTED_MODULE_0__.exec)("service nginx start"), ec("tmux kill-server"), ec(`pm2 delete ${name}`), ec("pm2 startup") ]), 
            needGenCert && await ec("pm2 restart all"), await Object(child_process_promise__WEBPACK_IMPORTED_MODULE_0__.exec)(`pm2 start "${__dirname}/index.js" --name ${name} --node-args="--max_old_space_size=${_utils_config__WEBPACK_IMPORTED_MODULE_7__.i}" --log-date-format 'DD.MM HH:mm:ss'`), 
            await Object(child_process_promise__WEBPACK_IMPORTED_MODULE_0__.exec)("pm2 save");
            const nginxconf = __webpack_require__(395)({
                SSL_CERT_LOCATION: _utils_ssl_utils__WEBPACK_IMPORTED_MODULE_9__.a,
                SSL_KEY_LOCATION: _utils_ssl_utils__WEBPACK_IMPORTED_MODULE_9__.b
            }), locationconf = __webpack_require__(396)({
                port,
                staticDir: Object(path__WEBPACK_IMPORTED_MODULE_5__.resolve)(__dirname, "./static/")
            });
            Object(fs__WEBPACK_IMPORTED_MODULE_2__.readFileSync)(nginxPath, "utf-8") !== nginxconf && Object(fs__WEBPACK_IMPORTED_MODULE_2__.writeFileSync)(nginxPath, nginxconf, "utf-8"), 
            Object(make_dir__WEBPACK_IMPORTED_MODULE_4__.sync)("/home/regulus-admin-nginx/"), 
            Object(fs__WEBPACK_IMPORTED_MODULE_2__.writeFileSync)(`/home/regulus-admin-nginx/${port}.conf`, locationconf), 
            await Object(child_process_promise__WEBPACK_IMPORTED_MODULE_0__.exec)("nginx -s reload"), 
            await Object(child_process_promise__WEBPACK_IMPORTED_MODULE_0__.exec)("ufw default deny incoming && ufw allow ssh && ufw allow http && ufw allow https && ufw logging off && ufw --force enable"), 
            process.exit(0);
        })().catch(err => console.error("(src/core/launcher.ts:129)", err));
    },
    395: function(module, exports, __webpack_require__) {
        var H = __webpack_require__(61);
        module.exports = function() {
            var T = new H.Template({
                code: function(c, p, i) {
                    var t = this;
                    return t.b(i = i || ""), t.b("map $http_upgrade $connection_upgrade {"), t.b("\n" + i), 
                    t.b("    default upgrade;"), t.b("\n" + i), t.b("    ''      close;"), t.b("\n" + i), 
                    t.b("}"), t.b("\n"), t.b("\n" + i), t.b("map $request_uri $request_path {"), t.b("\n" + i), 
                    t.b('    "~^(?<path>[^?]*)" $path;'), t.b("\n" + i), t.b("}"), t.b("\n"), t.b("\n" + i), 
                    t.b("map $request_path $last_request_path {"), t.b("\n" + i), t.b('    "~(?<path>[^/]*)$" $path;'), 
                    t.b("\n" + i), t.b("}"), t.b("\n"), t.b("\n" + i), t.b("server {"), t.b("\n" + i), 
                    t.b("    listen 80 default_server;"), t.b("\n" + i), t.b("    listen [::]:80 default_server ipv6only=on;"), 
                    t.b("\n" + i), t.b("    "), t.b("\n" + i), t.b("    listen 443 ssl default_server;"), 
                    t.b("\n" + i), t.b("    listen [::]:443 ssl default_server ipv6only=on;"), t.b("\n"), 
                    t.b("\n" + i), t.b("    gzip off;"), t.b("\n"), t.b("\n" + i), t.b("    server_tokens off;"), 
                    t.b("\n" + i), t.b("   "), t.b("\n" + i), t.b("    root /var/www/html;"), t.b("\n"), 
                    t.b("\n" + i), t.b("    server_name _;"), t.b("\n"), t.b("\n" + i), t.b("    ssl_certificate "), 
                    t.b(t.t(t.f("SSL_CERT_LOCATION", c, p, 0))), t.b(";"), t.b("\n" + i), t.b("    ssl_certificate_key "), 
                    t.b(t.t(t.f("SSL_KEY_LOCATION", c, p, 0))), t.b(";"), t.b("\n"), t.b("\n" + i), 
                    t.b('    if ($http_user_agent = ""){ '), t.b("\n" + i), t.b('        set $block "a";'), 
                    t.b("\n" + i), t.b("    }"), t.b("\n"), t.b("\n" + i), t.b("    if ($request_path !~ \\/hooks\\/){"), 
                    t.b("\n" + i), t.b('        set $block "${block}b";'), t.b("\n" + i), t.b("    }"), 
                    t.b("\n"), t.b("\n" + i), t.b('    if ($block = "ab") {'), t.b("\n" + i), t.b("        return 444;"), 
                    t.b("\n" + i), t.b("    }"), t.b("\n"), t.b("\n" + i), t.b("    if ($request_path ~* \\.php|\\.jsp|/\\.|form|cluster|mysql|policy|cgi-bin|admin|wp-|webdav|manager|localhost|loopback|security|127\\.0\\.0\\.1|\\.\\./\\.\\.){"), 
                    t.b("\n" + i), t.b("        return 444;"), t.b("\n" + i), t.b("    }"), t.b("\n"), 
                    t.b("\n" + i), t.b("    if ($request_method !~ ^(GET|POST|PATCH|PUT|OPTIONS|DELETE)$ ) {"), 
                    t.b("\n" + i), t.b("        return 444;"), t.b("\n" + i), t.b("    }"), t.b("\n"), 
                    t.b("\n" + i), t.b("    if ($http_user_agent ~* java|curl|wget|winhttp|HTTrack|clshttp|archiver|loader|email|harvest|extract|grab|miner|libwww-perl|curl|wget|python|nikto|scan|http-client){"), 
                    t.b("\n" + i), t.b("        return 444;"), t.b("\n" + i), t.b("    }"), t.b("\n"), 
                    t.b("\n" + i), t.b("    location / {"), t.b("\n" + i), t.b("        return 404;"), 
                    t.b("\n" + i), t.b("    }"), t.b("\n"), t.b("\n" + i), t.b("    include /home/regulus-admin-nginx/*.conf;"), 
                    t.b("\n" + i), t.b("}"), t.fl();
                },
                partials: {},
                subs: {}
            });
            return T.render.apply(T, arguments);
        };
    },
    396: function(module, exports, __webpack_require__) {
        var H = __webpack_require__(61);
        module.exports = function() {
            var T = new H.Template({
                code: function(c, p, i) {
                    var t = this;
                    return t.b(i = i || ""), t.b("location /"), t.b(t.t(t.f("port", c, p, 0))), t.b("/hooks/ {"), 
                    t.b("\n" + i), t.b('    if ($https = "") {'), t.b("\n" + i), t.b("        return 301 https://$host$request_uri;"), 
                    t.b("\n" + i), t.b("    }"), t.b("\n" + i), t.b("    proxy_pass         http://localhost:"), 
                    t.b(t.t(t.f("port", c, p, 0))), t.b(";"), t.b("\n" + i), t.b("    proxy_http_version 1.1;"), 
                    t.b("\n" + i), t.b("    proxy_set_header   Host              $host;"), t.b("\n" + i), 
                    t.b("    proxy_set_header   X-Forwarded-Host  $host;"), t.b("\n" + i), t.b("    proxy_set_header   X-Forwarded-For   $proxy_add_x_forwarded_for;"), 
                    t.b("\n" + i), t.b("    proxy_set_header   X-Real-IP         $remote_addr;"), t.b("\n" + i), 
                    t.b("    proxy_set_header   X-Forwarded-Proto $scheme;"), t.b("\n" + i), t.b("}"), 
                    t.b("\n"), t.b("\n" + i), t.b("location /"), t.b(t.t(t.f("port", c, p, 0))), t.b("/ws {"), 
                    t.b("\n" + i), t.b("    proxy_read_timeout 50s;"), t.b("\n" + i), t.b("    proxy_pass         http://localhost:"), 
                    t.b(t.t(t.f("port", c, p, 0))), t.b(";"), t.b("\n" + i), t.b("    proxy_http_version 1.1;"), 
                    t.b("\n" + i), t.b("    proxy_set_header   Upgrade $http_upgrade;"), t.b("\n" + i), 
                    t.b("    proxy_set_header   Connection $connection_upgrade;"), t.b("\n" + i), t.b("}"), 
                    t.b("\n"), t.b("\n" + i), t.b("location /"), t.b(t.t(t.f("port", c, p, 0))), t.b("/static/ {"), 
                    t.b("\n" + i), t.b("    root               "), t.b(t.t(t.f("staticDir", c, p, 0))), 
                    t.b(";"), t.b("\n" + i), t.b('    try_files          "/$last_request_path" =404;'), 
                    t.b("\n" + i), t.b("    gzip_static        on;"), t.b("\n" + i), t.b("    gzip_proxied       any;"), 
                    t.b("\n" + i), t.b("    gzip_types         *;"), t.b("\n" + i), t.b("    access_log         off;"), 
                    t.b("\n" + i), t.b("    etag               off;"), t.b("\n" + i), t.b('    add_header         Cache-Control "public, max-age=31536000";'), 
                    t.b("\n" + i), t.b("    sendfile           on;"), t.b("\n" + i), t.b("    sendfile_max_chunk 1m;"), 
                    t.b("\n" + i), t.b("    tcp_nopush         on;"), t.b("\n" + i), t.b("}"), t.b("\n"), 
                    t.b("\n" + i), t.b("location /"), t.b(t.t(t.f("port", c, p, 0))), t.b("/static/assets/ {"), 
                    t.b("\n" + i), t.b("    root               "), t.b(t.t(t.f("staticDir", c, p, 0))), 
                    t.b(";"), t.b("\n" + i), t.b('    try_files          "/assets/$last_request_path" =404;'), 
                    t.b("\n" + i), t.b("    gzip_static        on;"), t.b("\n" + i), t.b("    gzip_proxied       any;"), 
                    t.b("\n" + i), t.b("    gzip_types         *;"), t.b("\n" + i), t.b("    access_log         off;"), 
                    t.b("\n" + i), t.b("    etag               off;"), t.b("\n" + i), t.b('    add_header         Cache-Control "public, max-age=31536000";'), 
                    t.b("\n" + i), t.b("    sendfile           on;"), t.b("\n" + i), t.b("    sendfile_max_chunk 1m;"), 
                    t.b("\n" + i), t.b("    tcp_nopush         on;"), t.b("\n" + i), t.b("}"), t.b("\n"), 
                    t.b("\n" + i), t.b("location /"), t.b(t.t(t.f("port", c, p, 0))), t.b("/ {"), t.b("\n" + i), 
                    t.b("    if ($https) {"), t.b("\n" + i), t.b("        return 301 http://$host$request_uri;"), 
                    t.b("\n" + i), t.b("    }"), t.b("\n" + i), t.b("    proxy_pass         http://localhost:"), 
                    t.b(t.t(t.f("port", c, p, 0))), t.b(";"), t.b("\n" + i), t.b("    proxy_http_version 1.1;"), 
                    t.b("\n" + i), t.b("    proxy_set_header   Host              $host;"), t.b("\n" + i), 
                    t.b("    proxy_set_header   X-Forwarded-Host  $host;"), t.b("\n" + i), t.b("    proxy_set_header   X-Forwarded-For   $proxy_add_x_forwarded_for;"), 
                    t.b("\n" + i), t.b("    proxy_set_header   X-Real-IP         $remote_addr;"), t.b("\n" + i), 
                    t.b("    proxy_set_header   X-Forwarded-Proto $scheme;"), t.b("\n" + i), t.b("}"), 
                    t.fl();
                },
                partials: {},
                subs: {}
            });
            return T.render.apply(T, arguments);
        };
    },
    5: function(module, exports) {
        module.exports = require("fs");
    },
    6: function(module, exports) {
        module.exports = require("path");
    },
    61: function(module, exports) {
        module.exports = require("hogan.js");
    },
    77: function(module, exports) {
        module.exports = require("convict");
    },
    78: function(module, __webpack_exports__, __webpack_require__) {
        "use strict";
        var net__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(27), _cancelable_request__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(9);
        __webpack_exports__.a = async function() {
            let ip = "";
            try {
                const {body} = await Object(_cancelable_request__WEBPACK_IMPORTED_MODULE_1__.b)("GET", "http://ipecho.net/plain");
                if (ip = body.toString().trim(), !Object(net__WEBPACK_IMPORTED_MODULE_0__.isIP)(ip)) throw null;
            } catch (_) {
                try {
                    const {body} = await Object(_cancelable_request__WEBPACK_IMPORTED_MODULE_1__.b)("GET", "http://ident.me");
                    if (ip = body.toString().trim(), !Object(net__WEBPACK_IMPORTED_MODULE_0__.isIP)(ip)) throw null;
                } catch (_) {
                    const {body} = await Object(_cancelable_request__WEBPACK_IMPORTED_MODULE_1__.b)("GET", "http://icanhazip.com");
                    if (ip = body.toString().trim(), !Object(net__WEBPACK_IMPORTED_MODULE_0__.isIP)(ip)) throw "Invalid ip: " + ip;
                }
            }
            return ip;
        };
    },
    79: function(module, exports) {
        module.exports = require("request");
    },
    80: function(module, exports) {
        module.exports = require("ssl-validator");
    },
    81: function(module, exports) {
        module.exports = require("make-dir");
    },
    9: function(module, __webpack_exports__, __webpack_require__) {
        "use strict";
        var external_request_ = __webpack_require__(79);
        __webpack_require__.d(__webpack_exports__, "a", function() {
            return BaseRequestOpts;
        }), __webpack_require__.d(__webpack_exports__, "b", function() {
            return promiseRequest;
        });
        const BaseRequestOpts = Object.freeze({
            followRedirect: !0,
            followAllRedirects: !0,
            strictSSL: !1,
            jar: !1,
            timeout: 6e4
        }), client = external_request_.defaults({
            ...BaseRequestOpts,
            withCredentials: !0,
            gzip: !0,
            encoding: null,
            headers: {
                Accept: "*/*",
                "Cache-Control": "no-cache",
                Connection: "keep-alive",
                Pragma: "no-cache",
                "Upgrade-Insecure-Requests": "1",
                "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"
            }
        });
        function makeOpts(method, options) {
            return options.constructor === String ? {
                method,
                url: options
            } : Object.assign({
                method
            }, options);
        }
        function promiseRequest(method, options) {
            return new Promise((resolve, reject) => {
                client(makeOpts(method, options), (err, response) => {
                    err ? reject(err) : response.statusCode >= 500 ? reject({
                        code: response.statusCode,
                        message: response.statusMessage
                    }) : resolve(response);
                });
            });
        }
    }
});